﻿using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace HI5.PhysicsInteraction
{
    public class HandInteraction : MonoBehaviour
    {
        public bool m_IsGrabbing = false;

        [Tooltip("An amount to multiply the velocity of any objects being thrown. This can be useful when scaling up the play area to simulate being able to throw items further.")]
        public float throwMultiplier = 5f;

        public Transform[] m_IndexFingerTransforms;
        public Transform[] m_MiddleFingerTransforms;
        public Transform[] m_RingFingerTransforms;
        public Transform[] m_PinkyFingerTransforms;

        public float m_IndexFingerAngle;
        public float m_MiddleFingerAngle;
        public float m_RingFingerAngle;
        public float m_PinkyFingerAngle;

        public float m_IndexPercentage;
        public float m_MiddlePercentage;
        public float m_RingPercentage;
        public float m_PinkyPercentage;

        public Transform m_TrackingOriginTransform;

        private int m_GrabPercentage = 40;
        private int m_ReleasePercentage = 40;

        private float m_IndexAngleMin = 60f;
        private float m_IndexAngleMax = 150f;

        private float m_MiddleAngleMin = 60f;
        private float m_MiddleAngleMax = 150f;

        private float m_RingAngleMin = 60f;
        private float m_RingAngleMax = 150f;

        private float m_PinkyAngleMin = 60f;
        private float m_PinkyAngleMax = 150f;

        private void Start()
        {
            m_TrackingOriginTransform = FindObjectOfType<SteamVR_PlayArea>().transform;
        }

        void Update()
        {
            m_IndexFingerAngle = GetAngle(m_IndexFingerTransforms);
            m_MiddleFingerAngle = GetAngle(m_MiddleFingerTransforms);
            m_RingFingerAngle = GetAngle(m_RingFingerTransforms);
            m_PinkyFingerAngle = GetAngle(m_PinkyFingerTransforms);

            m_IndexPercentage = NormalizedAngle(m_IndexAngleMin, m_IndexAngleMax, m_IndexFingerAngle);
            m_MiddlePercentage = NormalizedAngle(m_IndexAngleMin, m_IndexAngleMax, m_MiddleFingerAngle);
            m_RingPercentage = NormalizedAngle(m_IndexAngleMin, m_IndexAngleMax, m_RingFingerAngle);
            m_PinkyPercentage = NormalizedAngle(m_IndexAngleMin, m_IndexAngleMax, m_PinkyFingerAngle);

            m_IsGrabbing = IsGrabbing();

            //TestThrowingVelocity();
        }

        float GetAngle(Transform[] fingerTransforms)
        {
            Vector3 left = fingerTransforms[0].position;
            Vector3 middle = fingerTransforms[1].position;
            Vector3 right = fingerTransforms[2].position;
            Vector3 vLeft = left - middle;
            Vector3 vRight = right - middle;

            return Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(vLeft, vRight) / (vLeft.magnitude * vRight.magnitude));
        }

        float NormalizedAngle(float angleMin, float angleMax, float angle)
        {
            float normalizedAngle, percantageValue;

            normalizedAngle = Mathf.InverseLerp(angleMax, angleMin, angle);
            percantageValue = (int)(normalizedAngle * 100);
            return percantageValue;
        }

        public bool IsGrabbing()
        {
            // If not grabbing and all fingers > grabPercentage
            if (!m_IsGrabbing &&
                m_MiddlePercentage > m_GrabPercentage &&
                m_RingPercentage > m_GrabPercentage &&
                m_PinkyPercentage > m_GrabPercentage)
            {
                return true;
            }
            // If grabbing and all fingers > releasePercentage
            else if (m_IsGrabbing &&
                (m_MiddlePercentage > m_ReleasePercentage ||
                m_RingPercentage > m_ReleasePercentage ||
                m_PinkyPercentage > m_ReleasePercentage))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void TestThrowingVelocity()
        {
            int index = 5;
            //int index = HI5_BindInfoManager.RightID;

            var deviceID = (SteamVR_TrackedObject.EIndex)index;
            Debug.Log("DEVICE ID INDEX IS: " + index);
            Debug.Log("DEVICE ID IS: " + deviceID);
            SteamVR_Behaviour_Pose device = new SteamVR_Behaviour_Pose();

            SteamVR_TrackedObject[] trackedObjects = Resources.FindObjectsOfTypeAll<SteamVR_TrackedObject>();
            Debug.Log("Found this many TRACKED OBJECTS: " + trackedObjects.Length);
            foreach (SteamVR_TrackedObject tracked in trackedObjects)
            {
                //Debug.Log("TRACKED INDEX IS: " + tracked.index);
                if (tracked.index == deviceID)
                {
                    Debug.Log("TRACKED INDEX FOUND");
                    if (tracked.gameObject.GetComponent<SteamVR_Behaviour_Pose>() != null)
                    {
                        Debug.Log("DEVICE BEHAVIOUR POSE FOUND");
                        device = tracked.gameObject.GetComponent<SteamVR_Behaviour_Pose>();
                    }
                }
            }

            if(device == null)
            {
                return;
            }

            //Vector3 velocity = new Vector3();
            //Vector3 angularVelocity = new Vector3();
            var velocity = device.GetVelocity();
            var angularVelocity = device.GetAngularVelocity();
            //var velocity = device.origin.TransformVector(device.GetVelocity());
            //var angularVelocity = device.origin.TransformVector(device.GetAngularVelocity());

            Debug.Log("DEVICE IS : " + device.name);

            //device.GetVelocitiesAtTimeOffset(0, out velocity, out angularVelocity);
            //velocity = m_TrackingOriginTransform.TransformVector(velocity);
            //angularVelocity = m_TrackingOriginTransform.TransformDirection(angularVelocity);

            Debug.Log("Velocity is: " + velocity.magnitude);
            Debug.Log("Angular Velocity is: " + angularVelocity.magnitude);

        }

        public void ThrowReleasedObject(Rigidbody _RigidBody, PhysicsHandGrabber _Hand)
        {
            var grabbingObject = _Hand.gameObject;

            bool throwVelocityWithAttachDistance = false;

            if (grabbingObject)
            {
                var grabbingObjectThrowMultiplier = throwMultiplier;

                Transform origin = null;// grabbingObject.transform;

                //var velocity = grabbingObject.transform.root.GetComponent<Rigidbody>().velocity;
                //var angularVelocity = grabbingObject.transform.root.GetComponent<Rigidbody>().angularVelocity;

                int index = _Hand.m_PhysicsHand.m_Hi5Hand.HandType == Hand.LEFT ? HI5_BindInfoManager.LeftID : HI5_BindInfoManager.RightID;

                //var device = SteamVR_Controller.Input(index);
                var deviceID = (SteamVR_TrackedObject.EIndex)index;
                //Debug.Log("DEVICE ID INDEX IS: " + index);
                //Debug.Log("DEVICE ID IS: " + deviceID);
                SteamVR_Behaviour_Pose device = new SteamVR_Behaviour_Pose();

                SteamVR_TrackedObject[] trackedObjects = Resources.FindObjectsOfTypeAll< SteamVR_TrackedObject>();
                Debug.Log("Found this many TRACKED OBJECTS: " + trackedObjects.Length);
                foreach (SteamVR_TrackedObject tracked in trackedObjects)
                {
                    //Debug.Log("TRACKED INDEX IS: " + tracked.index);
                    if (tracked.index == deviceID)
                    {
                        //Debug.Log("TRACKED INDEX FOUND");
                        if (tracked.GetComponent<SteamVR_Behaviour_Pose>() != null)
                        {
                            //Debug.Log("DEVICE BEHAVIOUR POSE FOUND");
                            device = tracked.GetComponent<SteamVR_Behaviour_Pose>();
                        }
                    }
                }

                var velocity = device.GetVelocity();
                var angularVelocity = device.GetAngularVelocity();

                //Debug.Log("DEVICE IS : " + device.name);

                Debug.Log("Velocity is: " + velocity.magnitude);
                Debug.Log("Angular Velocity is: " + angularVelocity.magnitude);

                if (origin != null)
                {
                    _RigidBody.velocity = origin.TransformVector(velocity) * (grabbingObjectThrowMultiplier * throwMultiplier);
                    _RigidBody.angularVelocity = origin.TransformDirection(angularVelocity);
                }
                else
                {
                    _RigidBody.velocity = velocity * (grabbingObjectThrowMultiplier * throwMultiplier);
                    _RigidBody.angularVelocity = angularVelocity;
                }

                if (throwVelocityWithAttachDistance)
                {
                    var rigidbodyCollider = _RigidBody.GetComponentInChildren<Collider>();
                    if (rigidbodyCollider)
                    {
                        Vector3 collisionCenter = rigidbodyCollider.bounds.center;
                        _RigidBody.velocity = _RigidBody.GetPointVelocity(collisionCenter + (collisionCenter - transform.position));
                    }
                    else
                    {
                        _RigidBody.velocity = _RigidBody.GetPointVelocity(_RigidBody.position + (_RigidBody.position - transform.position));
                    }
                }
            }
        }

    }
}
