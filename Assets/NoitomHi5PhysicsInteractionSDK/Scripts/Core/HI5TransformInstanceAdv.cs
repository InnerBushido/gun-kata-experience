﻿using UnityEngine;
using System.Collections.Generic;

namespace HI5.PhysicsInteraction
{
    public class HI5TransformInstanceAdv : HI5_TransformInstance
    {
        public SkinnedMeshRenderer m_HandMeshRenderer;
        public bool m_CollisionOccurred = false;
        //public HI5_PairManager m_PairManager;

        internal List<PhysicsCollidable> m_Collidables = new List<PhysicsCollidable>();

        public virtual void OnTriggerEnter(Collider other)
        {
            PhysicsCollidable collidable = other.GetComponent<PhysicsCollidable>();

            if (collidable != null)
            {
                m_CollisionOccurred = true;

                if (collidable != null)
                {
                    m_Collidables.Add(collidable);
                }
            }
        }
        
        public virtual void OnTriggerExit(Collider other)
        {
            PhysicsCollidable collidable = other.GetComponent<PhysicsCollidable>();

            if (collidable != null)
            {
                if (collidable != null && m_Collidables.Contains(collidable))
                {
                    m_Collidables.Remove(collidable);
                }

                if (m_Collidables.Count < 1)
                {
                    m_CollisionOccurred = false;
                }
            }
        }

        /// <summary>
        /// Added to fix problem when colliders are not automatically removed from list (through OnCollisionExit) 
        /// because the object is picked up or disabled
        /// </summary>
        /// <param name="_Collidable"></param>
        public virtual void RemoveAllCollidables(PhysicsCollidable _Collidable)
        {
            foreach (PhysicsCollidable c in _Collidable.m_AllCollidables)
            {
                RemoveCollidable(c);
            }
        }

        private void RemoveCollidable(PhysicsCollidable _Collidable)
        {
            while (m_Collidables.Contains(_Collidable))
            {
                m_Collidables.Remove(_Collidable);

            }
        }

    }
}
