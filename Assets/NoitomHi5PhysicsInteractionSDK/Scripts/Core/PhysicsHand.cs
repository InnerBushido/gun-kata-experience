﻿using UnityEngine;

namespace HI5.PhysicsInteraction
{
    public class PhysicsHand : MonoBehaviour
    {

        [Tooltip("Make physical hand teleport to correct position if the Hi5 Hand is no longer colliding in geometry")]
        public bool m_AllowHandToTeleport = true;

        [Tooltip("Used for checking whether hand should teleport through collidables")]
        public LayerMask m_CollidableLayers;

        [Tooltip("Bones attached to hand model")]
        public Transform[] m_HandBones;

        [Tooltip("Added Physical/Invisible colliders on fingers used for compound collider for hand collision")]
        public Transform[] m_CompoundFingerColliders;

        [Tooltip("Added to stabilize the hand so it doesn't get pulled by forces")]
        public Transform m_Arm;

        [Tooltip("Used to enable/disable when picked up by object like pistol which changes the parent of Hinge Joint fingers")]
        public Transform m_CompoundFingersParent;

        [Tooltip("Used for doing RigidBody physics with the hand")]
        public PhysicsRigidBody m_physicsRigidBody;

        [Tooltip("This is used for picking up objects using trigger collider on hand")]
        public PhysicsHandGrabber m_HandGrabber;

        public Transform m_CenterPivotOfHand;

        [HideInInspector]
        public HI5TransformInstanceAdv m_Hi5Hand;
        internal bool m_ForceHandPhysicsUpdate = false;

        private Rigidbody m_RigidBody;

        [SerializeField]
        private SkinnedMeshRenderer m_HandRenderer;

        // Variables used in MoveTowardsHand(), moved here to save on GC Processing
        private Vector3 directionToHand;
        private Vector3 directionVector;
        private float distanceToHand;
        private readonly float maxSeperationDistance = 0.01f;
        private readonly float distanceMagnitude = 0.01f;
        private Quaternion newRotation;
        private Vector3 newPosition;
        private bool collidableInPath = false;
        private float currentPositionDistance;

        enum DominantAxis
        {
            none,x,y,z
        }

        void Start()
        {
            m_physicsRigidBody.Initiate(this);

            if (m_Arm != null)
            {
                m_RigidBody = m_Arm.GetComponent<Rigidbody>();
            }
            else
            {
                m_RigidBody = m_physicsRigidBody.m_RigidBody;
            }

            m_HandRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        }

        void Update()
        {
            if (!PhysicsInteractionManager.Instance.m_UseSlowerButMoreAccurateCollisionPhysics &&
                !m_ForceHandPhysicsUpdate &&
                (!m_physicsRigidBody.m_CollisionOccurred && !m_Hi5Hand.m_CollisionOccurred))
            {
                SimpleUpdateHandPosition();
            }
        }

        void FixedUpdate()
        {
            // Always update finger rotations on FixedUpdate
            RotateFingerJoints();

            if (m_CompoundFingersParent != null && m_CompoundFingersParent.gameObject.activeSelf)
            {
                UpdateCompoundColliders();
            }

            if (PhysicsInteractionManager.Instance.m_UseSlowerButMoreAccurateCollisionPhysics ||
                m_ForceHandPhysicsUpdate ||
                (m_physicsRigidBody.m_CollisionOccurred || m_Hi5Hand.m_CollisionOccurred))
            {
                ComplexRigidHandUpdate();
            }
        }

        public void TogglePhysicsHand(bool toggleOn)
        {
            m_HandRenderer.enabled = toggleOn;
            m_physicsRigidBody.ToggleHandColliders(toggleOn);
        }

        void SimpleUpdateHandPosition()
        {
            m_RigidBody.velocity = Vector3.zero;
            m_RigidBody.angularVelocity = Vector3.zero;

            if (m_Arm != null)
            {
                m_Arm.position = m_Hi5Hand.HandBones[1].position;
                m_Arm.rotation = m_Hi5Hand.HandBones[1].rotation;
            }
            else
            {
                m_HandBones[1].position = m_Hi5Hand.HandBones[1].position;
                m_HandBones[1].rotation = m_Hi5Hand.HandBones[1].rotation;
            }
        }

        void UpdateFingersRotation()
        {
            for (int i = 2; i < m_HandBones.Length; i++)
            {
                m_HandBones[i].localRotation = m_Hi5Hand.HandBones[i].localRotation;
            }
        }

        void RotateFingerJoints()
        {
            for (int i = 2; i < m_HandBones.Length; i++)
            {
                ConfigurableJoint configurableJoint = m_HandBones[i].GetComponent<ConfigurableJoint>();

                if (configurableJoint != null)
                {
                    configurableJoint.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    configurableJoint.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

                    if (!configurableJoint.swapBodies)
                    {
                        configurableJoint.targetRotation = Quaternion.Inverse(m_Hi5Hand.HandBones[i].localRotation);
                    }
                    else
                    {
                        configurableJoint.targetRotation = Quaternion.Euler(Vector3.up * 90) * m_Hi5Hand.HandBones[i].localRotation * Quaternion.Euler(Vector3.up * -90);
                    }
                }
                else
                {
                    m_HandBones[i].localRotation = m_Hi5Hand.HandBones[i].localRotation;
                }
            }
        }

        void UpdateCompoundColliders()
        {
            for (int i = 2; i < m_CompoundFingerColliders.Length; i++)
            {
                m_CompoundFingerColliders[i].localRotation = m_Hi5Hand.HandBones[i].localRotation;
            }
        }

        void ComplexRigidHandUpdate()
        {
            m_RigidBody.velocity = Vector3.zero;
            m_RigidBody.angularVelocity = Vector3.zero;

            if (!m_physicsRigidBody.m_CollisionOccurred)
            {
                // No collision, use physics to move hand
                m_RigidBody.MovePosition(m_Hi5Hand.HandBones[1].position);
                m_RigidBody.MoveRotation(m_Hi5Hand.HandBones[1].rotation);
            }
            else if (
                (PhysicsInteractionManager.Instance.m_AllowHandsToTeleportThroughCollidables &&
                m_AllowHandToTeleport) &&
                !m_Hi5Hand.m_CollisionOccurred)
            {
                collidableInPath = CheckIfCollidableInPath(true);

                // If there is a collidable object in the way, teleport physical hand to hand position
                if (collidableInPath)
                {
                    // WARNING: This is very slow for some reason.
                    if (m_Arm != null)
                    {
                        m_Arm.position = m_Hi5Hand.HandBones[1].position;
                        m_Arm.rotation = m_Hi5Hand.HandBones[1].rotation;
                    }
                    else
                    {
                        m_HandBones[1].position = m_Hi5Hand.HandBones[1].position;
                        m_HandBones[1].rotation = m_Hi5Hand.HandBones[1].rotation;
                    }
                }
                else
                {
                    MoveTowardsHand();
                }
            }
            else
            {
                MoveTowardsHand();
            }
        }

        private void MoveTowardsHand()
        {
            // Do this if hand is embedded into geometry
            ApplySmoothingToRigidBodyValues();
            m_RigidBody.MovePosition(newPosition);
            m_RigidBody.MoveRotation(newRotation);
        }

        private void ApplySmoothingToRigidBodyValues()
        {
            // There is a collision, so move the hand a little toward Hi5 hand position
            distanceToHand = FindDistanceToHand(m_RigidBody.position);
            directionToHand = FindDirectionVectorTowardsHand(m_RigidBody.position);

            // If distance is far apart and rigidbody is colliding and Hi5 is colliding or there is a collidable in path (for when Hi5 goes through wall)
            //if (m_physicsRigidBody.m_CollisionOccurred && (m_Hi5Hand.m_CollisionOccurred || collidableInPath))
            if (distanceToHand > maxSeperationDistance &&
                m_physicsRigidBody.m_CollisionOccurred &&
                (m_Hi5Hand.m_CollisionOccurred || collidableInPath))
            {
                //newPosition = m_RigidBody.position + (directionToHand * distanceMagnitude);
                FigureOutNewHandPosition();
                //newPosition = m_RigidBody.position + (directionVector.normalized * distanceMagnitude);
                //newPosition = m_RigidBody.position + directionVector;
                newPosition = (m_RigidBody.position + directionVector) + (directionToHand * distanceMagnitude);

                currentPositionDistance = Vector3.Distance(m_RigidBody.position, newPosition);

                if (currentPositionDistance > 0.05f)
                {
                    //Debug.Log("DISTANCE IS GREATER THAN MAX: " + currentPositionDistance);
                    newPosition = m_RigidBody.position + (newPosition - m_RigidBody.position).normalized * 0.05f;
                }
            }
            else
            {
                newPosition = m_Hi5Hand.HandBones[1].position;
            }

            newRotation = m_Hi5Hand.HandBones[1].rotation;

            Debug.DrawLine(m_RigidBody.position, newPosition, Color.cyan);
        }

        private bool FigureOutNewHandPosition()
        {
            DominantAxis dominantAxis = DominantAxis.none;

            directionVector = m_Hi5Hand.HandBones[1].position - m_RigidBody.position;

            if (Mathf.Abs(directionVector.x) > Mathf.Abs(directionVector.y) && Mathf.Abs(directionVector.x) > Mathf.Abs(directionVector.z))
            {
                dominantAxis = DominantAxis.x;
            }
            else if (Mathf.Abs(directionVector.y) > Mathf.Abs(directionVector.x) && Mathf.Abs(directionVector.y) > Mathf.Abs(directionVector.z))
            {
                dominantAxis = DominantAxis.y;
            }
            else if (Mathf.Abs(directionVector.z) > Mathf.Abs(directionVector.x) && Mathf.Abs(directionVector.z) > Mathf.Abs(directionVector.y))
            {
                dominantAxis = DominantAxis.z;
            }

            if (dominantAxis == DominantAxis.x)
            {
                //directionVector = new Vector3(0, directionVector.y, directionVector.z);
                //if (CheckIfCollidableInPath(m_RigidBody.position, m_RigidBody.position + directionVector))
                //{
                //    directionVector = new Vector3(directionVector.x, directionVector.y, 0);
                //}
                directionVector = ReturnLongestDirectionVector(m_RigidBody.position, new Vector3(0, directionVector.y, directionVector.z), new Vector3(directionVector.x, directionVector.y, 0));
            }
            else if (dominantAxis == DominantAxis.y)
            {
                //directionVector = new Vector3(directionVector.x, 0, directionVector.z);
                //if (CheckIfCollidableInPath(m_RigidBody.position, m_RigidBody.position + directionVector))
                //{
                //    directionVector = new Vector3(0, directionVector.y, directionVector.z);
                //}
                directionVector = ReturnLongestDirectionVector(m_RigidBody.position, new Vector3(directionVector.x, 0, directionVector.z), new Vector3(0, directionVector.y, directionVector.z));
            }
            else if (dominantAxis == DominantAxis.z)
            {
                //directionVector = new Vector3(directionVector.x, directionVector.y, 0);
                //if (CheckIfCollidableInPath(m_RigidBody.position, m_RigidBody.position + directionVector))
                //{
                //    directionVector = new Vector3(directionVector.x, 0, directionVector.z);
                //}
                directionVector = ReturnLongestDirectionVector(m_RigidBody.position, new Vector3(directionVector.x, directionVector.y, 0), new Vector3(directionVector.x, 0, directionVector.z));
            }

            return false;

            //Debug.DrawLine(m_RigidBody.position, m_RigidBody.position + directionVector, Color.red);
        }

        private bool CheckIfCollidableInPath(bool _DrawDebug = false)
        {
            return CheckIfCollidableInPath(m_RigidBody.position, m_Hi5Hand.HandBones[1].position, _DrawDebug);
        }

        private bool CheckIfCollidableInPath(Vector3 startPosition, Vector3 endPosition, bool _DrawDebug = false)
        {
            float rayDistance = Vector3.Distance(startPosition, endPosition);
            Vector3 rayDirection = (endPosition - startPosition);
            Vector3 rayStartPosition = startPosition - (rayDirection.normalized * 0.01f);

            if (Physics.Raycast(rayStartPosition, rayDirection, rayDistance + 0.01f, m_CollidableLayers))
            {
                if (_DrawDebug)
                {
                    Debug.DrawRay(rayStartPosition, rayDirection, Color.red, 1);
                    Debug.Log("HIT COLLIDABLE");
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Given Two points (calculated from the hypotenus of the two rectangle created from the two points)
        /// Return the longest collision point since that is where we want the hand to move towards (away from the closest collision point).
        /// </summary>
        /// <param name="startPosition"></param>
        /// <param name="endDirectionVector"></param>
        /// <param name="endDirectionVector2"></param>
        /// <returns></returns>
        private Vector3 ReturnLongestDirectionVector(Vector3 startPosition, Vector3 endDirectionVector, Vector3 endDirectionVector2)
        {
            Vector3 endPosition = (startPosition + endDirectionVector);
            Vector3 rayDirection = (endPosition - startPosition);
            Vector3 rayStartPosition = startPosition - (rayDirection.normalized * 0.01f);
            RaycastHit hit;

            float firstDistance;
            float secondDistance;

            if (Physics.Raycast(rayStartPosition, rayDirection, out hit, 0.5f, m_CollidableLayers))
            {
                firstDistance = hit.distance;
                //Debug.DrawLine(rayStartPosition, hit.point, Color.white);
            }
            else
            {
                // No collision, go with initial prediction
                return endDirectionVector;
            }

            endPosition = (startPosition + endDirectionVector2);
            rayDirection = (endPosition - startPosition);
            rayStartPosition = startPosition - (rayDirection.normalized * 0.01f);

            if (Physics.Raycast(rayStartPosition, rayDirection, out hit, 0.5f, m_CollidableLayers))
            {
                secondDistance = hit.distance;
                //Debug.DrawLine(rayStartPosition, hit.point, Color.magenta);
            }
            else
            {
                // No collision, go with secondary prediction
                return endDirectionVector2;
            }

            // Return the longest collision distance
            if (firstDistance > secondDistance)
            {
                return endDirectionVector;
            }
            else
            {
                return endDirectionVector2;
            }

        }

        Vector3 FindDirectionVectorTowardsHand(Vector3 _CurrentHandPosition)
        {
            Vector3 directionVector;

            directionVector = m_Hi5Hand.HandBones[1].position - _CurrentHandPosition;
            directionVector.Normalize();

            return directionVector;
        }

        float FindDistanceToHand(Vector3 _CurrentHandPosition)
        {
            return (Vector3.Distance(_CurrentHandPosition, m_Hi5Hand.HandBones[1].position));
        }
    }
}