﻿using UnityEngine;

namespace HI5.PhysicsInteraction
{
    public class PhysicsRigidBodyFingers : MonoBehaviour
    {

        public bool m_IsRootOfFinger = false;
        public Collider m_FingerCollider;

        internal PhysicsRigidBody m_PhysicsRigidBody;
        internal Rigidbody m_RigidBody;
        internal HingeJoint m_HingeJoint;
        internal ConfigurableJoint m_ConfigurableJoint;
        internal Rigidbody m_ParentRigidBody;

        private Vector3 m_StartingPosition;
        private Quaternion m_StartingRotation;

        void Awake()
        {
            m_RigidBody = GetComponent<Rigidbody>();
            m_ParentRigidBody = transform.parent.GetComponent<Rigidbody>();
            m_HingeJoint = transform.GetComponent<HingeJoint>();
            m_ConfigurableJoint = transform.GetComponent<ConfigurableJoint>();

            m_StartingPosition = transform.localPosition;
            m_StartingRotation = transform.localRotation;
        }

        public virtual void OnCollisionEnter(Collision c)
        {
            if (m_PhysicsRigidBody != null)
            {
                m_PhysicsRigidBody.CheckWhetherToAddCollision(c);
            }
        }

        public virtual void OnCollisionExit(Collision c)
        {
            if (m_PhysicsRigidBody != null)
            {
                m_PhysicsRigidBody.CheckWhetherToRemoveCollision(c);
            }
        }

        /// <summary>
        /// Default to RootOfFinger
        /// </summary>
        public void ReassignHingeJointConnectBody()
        {
            ReassignJointConnectBody(m_ParentRigidBody);
        }

        /// <summary>
        /// Reassign Hinge Joint Connected Body
        /// </summary>
        /// <param name="_RigidBodyToChangeTo"></param>
        public void ReassignJointConnectBody(Rigidbody _RigidBodyToChangeTo)
        {
            if (m_HingeJoint != null)
            {
                // Needed to fix bug where reassigning connectedBody sets rotation values
                transform.localPosition = m_StartingPosition;
                transform.localRotation = m_StartingRotation;

                m_HingeJoint.connectedBody = _RigidBodyToChangeTo;
            }
            else if (m_ConfigurableJoint != null)
            {
                // Needed to fix bug where reassigning connectedBody sets rotation values
                transform.localPosition = m_StartingPosition;
                transform.localRotation = m_StartingRotation;

                m_ConfigurableJoint.connectedBody = _RigidBodyToChangeTo;
            }
        }
    }
}
