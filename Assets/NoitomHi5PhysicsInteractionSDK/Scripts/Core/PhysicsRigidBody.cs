﻿using System.Collections.Generic;
using UnityEngine;

namespace HI5.PhysicsInteraction
{
    [RequireComponent(typeof(Rigidbody))]
    public class PhysicsRigidBody : MonoBehaviour
    {
        [Tooltip("Whether hand is currently colliding with a collidable object")]
        public bool m_CollisionOccurred = false;
        
        [Tooltip("Used for toggling finger colliders and assigning physics material")]
        public GameObject[] m_FingerColliders;

        internal Rigidbody m_RigidBody;

        private PhysicsHand m_CurrentHand;

        [SerializeField]
        internal List<PhysicsCollidable> m_Collidables = new List<PhysicsCollidable>();
        [SerializeField]
        internal List<Collider> m_HandColliders = new List<Collider>();

        private List<PhysicsRigidBodyFingers> m_RigidBodyFingers = new List<PhysicsRigidBodyFingers>();
        private List<PhysicsRigidBodyFingers> m_RigidBodyFingersRoot = new List<PhysicsRigidBodyFingers>();

        internal List<Collider> m_AllColliders;
        internal List<Collider> m_ActiveColliders;

        private bool m_Hi5HandVibrating = false;
        private float m_VibrateTime = 0.1f;

        public enum CollidableType
        {
            None,
            Collidable,
            CollisionName
        }

        public void Initiate(PhysicsHand currentHand)
        {
            m_RigidBody = GetComponent<Rigidbody>();
            m_CurrentHand = currentHand;
            m_AllColliders = new List<Collider>();

            GetComponentsInChildren(false, m_RigidBodyFingers);
            GetComponentsInChildren(false, m_AllColliders);

            m_ActiveColliders = new List<Collider>(m_AllColliders);

            for (int i = 0; i < m_ActiveColliders.Count; i++)
            {
                if (!m_ActiveColliders[i].enabled || m_ActiveColliders[i].isTrigger)
                {
                    m_ActiveColliders.Remove(m_ActiveColliders[i]);
                }
            }

            foreach (PhysicsRigidBodyFingers finger in m_RigidBodyFingers)
            {
                finger.m_PhysicsRigidBody = this;

                if (finger.m_IsRootOfFinger)
                {
                    m_RigidBodyFingersRoot.Add(finger);
                }
            }

            foreach(GameObject root in m_FingerColliders)
            {
                if (PhysicsInteractionManager.Instance.m_UseRoundedFingerColliders)
                {
                    Collider fingerCollider = root.GetComponent<CapsuleCollider>();

                    root.GetComponent<BoxCollider>().enabled = false;

                    fingerCollider.enabled = true;
                    fingerCollider.material = PhysicsInteractionManager.Instance.m_FingerPhysicsMaterial;
                }
            }
        }

        public virtual void OnCollisionEnter(Collision c)
        {
            CheckWhetherToAddCollision(c);
        }

        public virtual void OnCollisionExit(Collision c)
        {
            CheckWhetherToRemoveCollision(c);
        }

        public void ToggleHandColliders(bool toggleOn)
        {
            foreach (Collider collider in m_ActiveColliders)
            {
                collider.gameObject.SetActive(toggleOn);
            }
        }

        /// <summary>
        /// Fail safe to reset hand and make it not colliding with any objects.
        /// </summary>
        public void ClearAllCollisions()
        {
            m_Collidables.Clear();
            m_HandColliders.Clear();
            CheckWhetherHandFinishedColliding();
        }

        /// <summary>
        /// When object is picked up, we assign root of finger RigidBody to be on finger rather than on hand.
        /// This is done so that any pressure applied to finger (ex. by pistol trigger) does not move the hand itself
        /// </summary>
        /// <param name="_UseHandAsBody">Whether to use hand as root of hinge joint or finger root</param>
        /// <param name="_FingerRigidBody">Finger To Reassign</param>
        public virtual void ReassignJointRootConnectedBody(bool _UseHandAsBody, PhysicsRigidBodyFingers _FingerRigidBody)
        {
            if (_UseHandAsBody)
            {
                _FingerRigidBody.ReassignJointConnectBody(m_RigidBody);
            }
            else
            {
                _FingerRigidBody.ReassignHingeJointConnectBody();
            }
        }

        /// <summary>
        /// When object is picked up, we assign root of finger RigidBody to be on finger rather than on hand.
        /// This is done so that any pressure applied to finger (ex. by pistol trigger) does not move the hand itself
        /// This will reassign all Finger Roots
        /// </summary>
        /// <param name="_UseHandAsBody">Whether to use hand as root of hinge joint or finger root</param>
        public virtual void ReassignAllHingeJointRootConnectedBody(bool _UseHandAsBody)
        {
            foreach (PhysicsRigidBodyFingers finger in m_RigidBodyFingersRoot)
            {
                ReassignJointRootConnectedBody(_UseHandAsBody, finger);
            }
        }

        public virtual void ReassignJointLayersName(bool _ObjectHeld)
        {
            foreach (Collider collider in m_ActiveColliders)
            {
                if (_ObjectHeld)
                {
                    if (collider != null)
                    {
                        collider.gameObject.layer = LayerMask.NameToLayer("HandCollidersHeld");
                    }
                }
                else
                {
                    if (collider != null)
                    {
                        collider.gameObject.layer = LayerMask.NameToLayer("HandColliders");
                    }
                }
            }
        }

        public CollidableType ReturnCollidableType(Collision c, ref PhysicsCollidable collidable)
        {
            string collisionName = "";

            // If collidable is not on collider, check rigidbody
            if (collidable == null)
            {
                collidable = c.rigidbody.GetComponent<PhysicsCollidable>();
            }

            collisionName = LayerMask.LayerToName(c.collider.gameObject.layer);

            if (collidable != null)
            {
                return CollidableType.Collidable;
            }
            else if (collisionName == "HandColliders" || collisionName == "GrabbableInHand")
            {
                return CollidableType.CollisionName;
            }
            else
            {
                return CollidableType.None;
            }
        }

        public bool CheckWhetherToAddCollision(Collision c)
        {
            PhysicsCollidable collidable = c.collider.GetComponent<PhysicsCollidable>();
            bool collisionOccurredThisFrame = false;
            CollidableType collisionType;

            collisionType = ReturnCollidableType(c, ref collidable);

            if (collisionType == CollidableType.Collidable)
            {
                // Do not add to list if we are currently holding the object
                if (!m_CurrentHand.m_HandGrabber.m_CurrentlyHoldingGrabbable ||
                    !CheckAllCollidablesIfHeldInHand(collidable,
                    m_CurrentHand.m_HandGrabber.m_CurrentHeldGrabbable))
                {
                    m_CollisionOccurred = true;
                    collisionOccurredThisFrame = true;
                    m_Collidables.Add(collidable);
                }

            }
            else if (collisionType == CollidableType.CollisionName)
            {
                m_CollisionOccurred = true;
                collisionOccurredThisFrame = true;
                m_HandColliders.Add(c.collider);
            }

            if (m_CollisionOccurred)
            {
                HandStartedColliding();
            }

            if (collisionOccurredThisFrame)
            {
                return true;
            }

            return false;
        }

        public bool CheckWhetherToRemoveCollision(Collision c)
        {
            PhysicsCollidable collidable = c.collider.GetComponent<PhysicsCollidable>();
            string collisionName = "";
            bool collisionRemoved = false;

            // If collidable is not on collider, check rigidbody
            if (collidable == null)
            {
                collidable = c.rigidbody.GetComponent<PhysicsCollidable>();
            }
            collisionName = LayerMask.LayerToName(c.collider.gameObject.layer);

            if (collidable != null || collisionName == "HandColliders" || collisionName == "GrabbableInHand")
            {
                if (collidable != null)
                {
                    if (m_Collidables.Contains(collidable))
                    {
                        m_Collidables.Remove(collidable);
                        collisionRemoved = true;
                    }
                }
                else if ((collisionName == "HandColliders" || collisionName == "GrabbableInHand") && m_HandColliders.Contains(c.collider))
                {
                    m_HandColliders.Remove(c.collider);
                    collisionRemoved = true;
                }

                CheckWhetherHandFinishedColliding();

                if (collisionRemoved)
                {
                    return true;
                }
            }

            return false;
        }

        public void CheckWhetherHandFinishedColliding()
        {
            if (m_Collidables.Count < 1 && m_HandColliders.Count < 1)
            {
                m_CollisionOccurred = false;
                HandEndedColliding();
            }
        }

        /// <summary>
        /// Check if collidable is held in hand (or child of parent held in hand)
        /// </summary>
        /// <param name="_Collidable"></param>
        /// <returns></returns>
        public bool CheckAllCollidablesIfHeldInHand(PhysicsCollidable _Collidable, PhysicsGrabbable _CurrentlyHeldGrabbable)
        {
            foreach (PhysicsCollidable c in _CurrentlyHeldGrabbable.m_AllCollidables)
            {
                if (c == _Collidable)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Added to fix problem when colliders are not automatically removed from 
        /// list (through OnCollisionExit) because the object is picked up or disabled
        /// </summary>
        /// <param name="_Collidable"></param>
        public void RemoveAllCollidables(PhysicsCollidable _Collidable)
        {
            foreach (PhysicsCollidable c in _Collidable.m_AllCollidables)
            {
                RemoveCollidable(c);
            }

            CheckWhetherHandFinishedColliding();
        }

        public void RemoveCollidable(PhysicsCollidable _Collidable)
        {
            while (m_Collidables.Contains(_Collidable))
            {
                m_Collidables.Remove(_Collidable);
            }
        }

        public void RemoveCollidable(Collider _Collider)
        {
            while (m_HandColliders.Contains(_Collider))
            {
                m_HandColliders.Remove(_Collider);
            }
        }

        protected virtual void HandStartedColliding()
        {
            PhysicsInteractionManager.Instance.ShowHandsDuringCollision(true, m_CurrentHand.m_Hi5Hand);

            if (PhysicsInteractionManager.Instance.m_HandsVibrateOnCollision && !m_Hi5HandVibrating)
            {
                m_Hi5HandVibrating = true;
                VibrateHi5Hand();
            }
        }

        protected virtual void HandEndedColliding()
        {
            m_CollisionOccurred = false;

            PhysicsInteractionManager.Instance.ShowHandsDuringCollision(false, m_CurrentHand.m_Hi5Hand);
            m_Hi5HandVibrating = false;
        }

        private void VibrateHi5Hand()
        {
            PhysicsInteractionManager.Instance.VibrateHi5Hand(m_CurrentHand.m_Hi5Hand, m_VibrateTime);
        }
    }
}
