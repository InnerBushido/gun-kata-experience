﻿using UnityEngine;

namespace HI5.PhysicsInteraction
{
    /// <summary>
    /// IsTrigger Collidable object used for picking up PhysicsGrabbable objects
    /// </summary>
    public class PhysicsHandGrabber : MonoBehaviour
    {
        public PhysicsHand m_PhysicsHand;

        [Tooltip("Where objects are snapped to when picked up")]
        public Transform m_CenterOfHand;

        internal bool m_CurrentlyHoldingGrabbable;

        internal HandInteraction m_HandInteraction;

        internal PhysicsGrabbable m_CurrentHeldGrabbable;

        BoxCollider m_HandCollider;

        protected virtual void Start()
        {
            m_HandCollider = GetComponent<BoxCollider>();
            m_HandInteraction = GetComponent<HandInteraction>();
        }

        protected virtual void Update()
        {
            if (m_CurrentHeldGrabbable != null && !m_HandInteraction.m_IsGrabbing)
            {
                Release();
            }
        }

        public virtual void Grab(PhysicsGrabbable _Grabbable)
        {
            m_CurrentlyHoldingGrabbable = true;
            m_CurrentHeldGrabbable = _Grabbable;
            _Grabbable.PickUpGrabbable(this);

            m_PhysicsHand.m_physicsRigidBody.RemoveAllCollidables(_Grabbable);
            m_PhysicsHand.m_Hi5Hand.RemoveAllCollidables(_Grabbable);
        }

        public virtual void Release()
        {
            if (m_CurrentHeldGrabbable != null)
            {
                m_CurrentHeldGrabbable.RemoveAllCurrentCollidablesFromHand();
                m_CurrentHeldGrabbable.ReleaseGrabbable(this);
            }

            m_CurrentHeldGrabbable = null;
            m_CurrentlyHoldingGrabbable = false;
        }

        public virtual void OnTriggerStay(Collider other)
        {
            if (m_CurrentHeldGrabbable == null && m_HandInteraction.m_IsGrabbing)
            {
                PhysicsGrabbable _Grabbable;
                _Grabbable = RecursivelyFindGrabbable(other.transform);

                if (_Grabbable != null)
                {
                    if (_Grabbable.m_CanBeGrabbed && _Grabbable.m_IsGrabbable)
                    {
                        Grab(_Grabbable);
                    }
                }
                else
                {
                    //Debug.Log(
                    //    "Error: Couldn't Recursively find PhysicsGrabbable Script on collider: "
                    //    + other.name + ". Make sure to have scipt on root of gameobject.");
                }
            }
        }
        
        private PhysicsGrabbable RecursivelyFindGrabbable(Transform other)
        {
            PhysicsGrabbable _Grabbable = other.GetComponent<PhysicsGrabbable>();

            // If collidable is not on collider, check parent
            if (_Grabbable == null)
            {
                if (other.transform.parent != null)
                {
                    _Grabbable = RecursivelyFindGrabbable(other.transform.parent);
                }
            }

            return _Grabbable;
        }

        public virtual void GrabbableGrabbed() { }

        public virtual void GrabbableReleased() { }

    }
}