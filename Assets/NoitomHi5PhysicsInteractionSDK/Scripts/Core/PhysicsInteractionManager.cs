﻿using UnityEngine;
using Utilities;

namespace HI5.PhysicsInteraction
{
    public class PhysicsInteractionManager : SingletonBehaviour<PhysicsInteractionManager>
    {

        public HI5TransformInstanceAdv leftHand;
        public HI5TransformInstanceAdv rightHand;

        public PhysicsHand leftPhyicsHandPrefab;
        public PhysicsHand rightPhyicsHandPrefab;

        public PhysicMaterial m_FingerPhysicsMaterial;

        [Tooltip("If set to true on start, collision hands will be shown when hands collide with collidable")]
        public bool m_StartShowCollisionHands = false;

        [Tooltip("Changes will need to be made in Physics Matrix")]
        public bool m_AllowHandsToCollideWithOtherHands = false;

        [Tooltip("Changes will need to be made in Physics Matrix")]
        public bool m_HeldObjectsCollideWithHands = true;

        [Tooltip("Hand will vibrate a little when colliding with a collidable")]
        public bool m_HandsVibrateOnCollision = true;

        [Tooltip("Slightly Slows down the hand so that it is always running in FixedUpdate")]
        public bool m_UseSlowerButMoreAccurateCollisionPhysics = false;

        [Tooltip("Allow hands to teleport to where they should be if the hand and Hi5Hand are not colliding.")]
        public bool m_AllowHandsToTeleportThroughCollidables = true;
        
        [Tooltip("When a grabbable is dropped, this will ensure that no collidables get left behind on hand.")]
        public bool m_ResetHandCollisionsAfterReleasingGrabbable = false;

        [Tooltip("Whether to use capsule colliders for fingers or box colliders")]
        public bool m_UseRoundedFingerColliders = false;

        [Tooltip("Allow fingers to rotate in Y-Axis (Yaw)")]
        public bool m_UseFreeMovingFingers = true;

        public const float m_HandCollisionVibrationTime = 0.2f/*s*/;

        public bool ShowCollisionHands
        {
            get { return m_ShowCollisionHands; }
            set
            {
                leftHand.m_HandMeshRenderer.enabled = value;
                rightHand.m_HandMeshRenderer.enabled = value;
                m_ShowCollisionHands = value;
            }
        }

        private bool m_ShowCollisionHands = true;

        private PhysicsHand leftPhysicsHand;
        private PhysicsHand rightPhysicsHand;

        void Start()
        {
            if (m_StartShowCollisionHands)
            {
                m_ShowCollisionHands = true;
            }
            else
            {
                m_ShowCollisionHands = false;
            }

            ReplaceHandsWithPhysicsHands();

            if(m_UseFreeMovingFingers)
            {
                HI5.HI5_Manager.EnableFingerAdbFixed(!m_UseFreeMovingFingers);
            }
        }

        void RotateTwoHandUpdatePosition()
        {
            Vector3 GunRightAnchor = Vector3.zero;
            Vector3 GunLeftAnchor = Vector3.zero;
            Vector3 RightHandAnchor = Vector3.zero;
            Vector3 LeftHandAnchor = Vector3.zero;

            Vector3 GunDirection = GunRightAnchor - GunLeftAnchor;
            Vector3 HandsDirection = RightHandAnchor - LeftHandAnchor;

            GameObject Gun = null;
            Quaternion r = Quaternion.FromToRotation(GunDirection, HandsDirection);
            Gun.transform.rotation = r;

            Gun.transform.position += RightHandAnchor - GunRightAnchor;
        }

        public void ReplaceHandsWithPhysicsHands()
        {
            leftHand.m_HandMeshRenderer.enabled = false;
            rightHand.m_HandMeshRenderer.enabled = false;

            leftPhysicsHand = Instantiate(leftPhyicsHandPrefab, leftHand.transform.position, leftHand.transform.rotation);
            rightPhysicsHand = Instantiate(rightPhyicsHandPrefab, rightHand.transform.position, rightHand.transform.rotation);

            leftPhysicsHand.m_Hi5Hand = leftHand;
            rightPhysicsHand.m_Hi5Hand = rightHand;
        }

        public void ShowHandsDuringCollision(bool _ShowHand, HI5TransformInstanceAdv _Hand)
        {
            if (ShowCollisionHands)
            {
                _Hand.m_HandMeshRenderer.enabled = _ShowHand;
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.J))
            {
                HI5_Manager.RightHandVibrate(1000);
            }
            else if (Input.GetKey(KeyCode.K))
            {
                HI5_Manager.RightHandVibrate(100);
            }
            else if (Input.GetKeyDown(KeyCode.L))
            {
                HI5_Manager.RightHandVibrate(100);
            }

            if (Input.GetKeyDown(KeyCode.G))
            {
                m_ShowCollisionHands = !m_ShowCollisionHands;
            }
            else if(Input.GetKeyDown(KeyCode.F))
            {
                m_UseFreeMovingFingers = !m_UseFreeMovingFingers;
                HI5.HI5_Manager.EnableFingerAdbFixed(m_UseFreeMovingFingers);
            }
        }

        public void VibrateHi5Hand(HI5TransformInstanceAdv _Hand, float _TimeInSeconds = m_HandCollisionVibrationTime)
        {
            int timeInMilliseconds = (int)(_TimeInSeconds * 1000);

            if (_Hand.HandType == HI5.Hand.LEFT)
            {
                HI5_Manager.LeftHandVibrate(timeInMilliseconds);
            }
            else
            {
                HI5_Manager.RightHandVibrate(timeInMilliseconds);
            }
        }
    }
}
