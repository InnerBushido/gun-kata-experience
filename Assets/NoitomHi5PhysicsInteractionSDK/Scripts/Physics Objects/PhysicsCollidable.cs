﻿using System.Collections.Generic;
using UnityEngine;

namespace HI5.PhysicsInteraction
{
    public class PhysicsCollidable : MonoBehaviour
    {
        internal List<Collider> m_AllColliders;
        internal List<Collider> m_ActiveColliders;
        internal List<PhysicsCollidable> m_AllCollidables;

        public virtual void Start()
        {
            m_AllColliders = new List<Collider>();
            m_AllCollidables = new List<PhysicsCollidable>();

            transform.GetComponentsInChildren(false, m_AllColliders);
            transform.GetComponentsInChildren(false, m_AllCollidables);

            m_ActiveColliders = new List<Collider>(m_AllColliders);

            for (int i = 0; i < m_ActiveColliders.Count; i++)
            {
                if (!m_ActiveColliders[i].enabled)
                {
                    m_ActiveColliders.Remove(m_ActiveColliders[i]);
                }
            }
        }
    }
}
