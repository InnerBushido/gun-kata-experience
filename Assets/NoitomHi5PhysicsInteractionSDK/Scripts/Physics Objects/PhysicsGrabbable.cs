﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HI5.PhysicsInteraction
{
    public class PhysicsGrabbable : PhysicsCollidable
    {
        [Tooltip("Whether object can be grabbed")]
        public bool m_IsGrabbable = true;

        [Tooltip("Enable gravity so that grabbable pulls down hand when held due to weight")]
        public bool m_EnableGravityWhenHeld = false;

        [Tooltip("Used for toggling collision layers on collidables")]
        public List<Collider> m_PickUpPhysicalColliders;

        [Tooltip("Used for picking up Grabbables")]
        public Collider m_PickUpTriggerCollider;

        [Tooltip("Grabbable will go through physics geometry.")]
        public bool m_MakeGrabbableKinematicWhenGrabbed = false;

        [Tooltip("When grabbed, make grabbable a child of hand and remove rigidbody, otherwise use Joint to lock to hand which is better for physics simulation.")]
        public bool m_MakeGrabbableChildOfHandWhenGrabbed = true;

        [Tooltip("Used for objects that need to keep their rotation relative when picked up depending on which hand picks it up.")]
        public bool m_FlipGrabbableWhenHeld = true;

        [Tooltip("These components will be transferred to the hand when the grabbable is picked up. Needed for OnCollision calls to be called correctly on grabbable.")]
        public List<Component> m_OnCollisionComponents;

        public Transform m_OffsetGrabLocation;

        // Added for checking whether grabbable is colliding or not based on Hi5TransformInstanceAdv
        public bool m_CollisionOccurred = false;

        //public Vector3 m_AddRotationOffsetWhenHeld = new Vector3();

        internal bool m_IsGrabbed = false;
        internal bool m_CanBeGrabbed = true;
        internal PhysicsHandGrabber m_CurrentHandGrabbing;
        internal Rigidbody m_RigidBody;
        internal Transform m_OriginalParent;
        
        internal List<PhysicsCollidable> m_Collidables = new List<PhysicsCollidable>();
        internal List<Collider> m_HandColliders = new List<Collider>();
        
        // Save the transform/rigidbody for use when removing rigidbody for correct world collisions when grabbed.
        protected RigidbodyState m_SavedRigidBody;
        private Transform m_RigidBodyTransform;
        private PhysicsRigidBody m_CurrentHandGrabbingRigidBody;

        protected ConfigurableJoint m_ConfigJoint;
        private bool m_StartedWithConfigJoint;

        protected const float m_TimeUntilCollidable = 0.2f;

        private List<Component> m_OnCollisionComponentsOnRigidbody;

        public override void Start()
        {
            base.Start();

            // Find Physical Colliders
            if (m_PickUpPhysicalColliders.Count < 1)
            {
                foreach (Collider c in m_AllColliders)
                {
                    if (!c.isTrigger)
                    {
                        m_PickUpPhysicalColliders.Add(c);
                    }
                }
            }

            // Find Trigger Collider
            if (m_PickUpTriggerCollider == null)
            {
                foreach (Collider c in m_AllColliders)
                {
                    if (c.isTrigger)
                    {
                        m_PickUpTriggerCollider = c;
                        break;
                    }
                }
            }

            m_RigidBody = GetComponent<Rigidbody>();
            if (m_RigidBody == null)
            {
                Debug.Log("ERROR: YOU MUST HAVE RIGIDBODY ATTACHED TO ROOT OF GRABBABLE OBJECT");
            }

            m_RigidBodyTransform = m_RigidBody.transform;
            m_OriginalParent = transform.parent;

            m_ConfigJoint = GetComponent<ConfigurableJoint>();
        }

        //public virtual void Update()
        //{
        //    if (m_CurrentHandGrabbing != null)
        //    {
        //        if (m_CurrentHandGrabbing.m_PhysicsHand.m_Hi5Hand.HandType == Hand.LEFT)
        //        {
        //            if (m_OffsetGrabLocation != null)
        //            {
        //                Vector3 offsetPosition = m_OffsetGrabLocation.localPosition;
        //                Vector3 offsetRotation = m_OffsetGrabLocation.localRotation.eulerAngles;
        //                m_RigidBody.transform.localPosition = new Vector3(-offsetPosition.x, offsetPosition.y, offsetPosition.z);
        //                m_RigidBody.transform.localRotation = Quaternion.Euler(offsetRotation.x, -offsetRotation.y, -offsetRotation.z);
        //            }
        //        }
        //        else
        //        {
        //            if (m_OffsetGrabLocation != null)
        //            {
        //                m_RigidBody.transform.localPosition = m_OffsetGrabLocation.localPosition;
        //                m_RigidBody.transform.localRotation = m_OffsetGrabLocation.localRotation;
        //            }
        //        }
        //    }
        //}

        public virtual void OnCollisionEnter(Collision c)
        {
            if (m_CurrentHandGrabbingRigidBody != null)
            {
                if (m_CurrentHandGrabbingRigidBody.CheckWhetherToAddCollision(c))
                {
                    PhysicsCollidable collidable = c.collider.GetComponent<PhysicsCollidable>();
                    PhysicsRigidBody.CollidableType collidableType;

                    collidableType = m_CurrentHandGrabbingRigidBody.ReturnCollidableType(c, ref collidable);

                    if (collidableType == PhysicsRigidBody.CollidableType.Collidable)
                    {
                        AddCollidable(collidable);
                    }
                    else if (collidableType == PhysicsRigidBody.CollidableType.CollisionName)
                    {
                        AddCollidable(c.collider);
                    }
                }

            }
        }

        public virtual void OnCollisionExit(Collision c)
        {
            if (m_CurrentHandGrabbingRigidBody != null)
            {
                if (m_CurrentHandGrabbingRigidBody.CheckWhetherToRemoveCollision(c))
                {
                    PhysicsCollidable collidable = c.collider.GetComponent<PhysicsCollidable>();
                    PhysicsRigidBody.CollidableType collidableType;

                    collidableType = m_CurrentHandGrabbingRigidBody.ReturnCollidableType(c, ref collidable);

                    if (collidableType == PhysicsRigidBody.CollidableType.Collidable)
                    {
                        RemoveCollidable(collidable);
                    }
                    else if (collidableType == PhysicsRigidBody.CollidableType.CollisionName)
                    {
                        RemoveCollidable(c.collider);
                    }
                }
            }
        }

        public virtual void OnCollisionStay(Collision c)
        {
            m_CollisionOccurred = true;
        }

        public virtual void FixedUpdate()
        {
            m_CollisionOccurred = false;
        }

        public void AddCollidable(PhysicsCollidable c)
        {
            m_Collidables.Add(c);
        }

        public void AddCollidable(Collider c)
        {
            m_HandColliders.Add(c);
        }

        public void RemoveCollidable(PhysicsCollidable c)
        {
            if (m_Collidables.Contains(c))
            {
                m_Collidables.Remove(c);
            }
        }

        public void RemoveCollidable(Collider c)
        {
            if (m_HandColliders.Contains(c))
            {
                m_HandColliders.Remove(c);
            }
        }

        public void RemoveAllCurrentCollidablesFromHand()
        {
            if (m_CurrentHandGrabbingRigidBody != null)
            {
                foreach (PhysicsCollidable c in m_Collidables)
                {
                    m_CurrentHandGrabbingRigidBody.RemoveCollidable(c);
                }

                foreach (Collider c in m_HandColliders)
                {
                    m_CurrentHandGrabbingRigidBody.RemoveCollidable(c);
                }

                m_CurrentHandGrabbingRigidBody.CheckWhetherHandFinishedColliding();
                m_Collidables.Clear();
                m_HandColliders.Clear();
            }
        }

        public virtual void PickUpGrabbable(PhysicsHandGrabber bindToHand)
        {
            Debug.Log("PICKING UP GRABBABLE: " + gameObject.name);

            m_CurrentHandGrabbing = bindToHand;

            if (m_FlipGrabbableWhenHeld)
            {
                if (m_CurrentHandGrabbing.m_PhysicsHand.m_Hi5Hand.HandType == Hand.LEFT)
                {
                    m_RigidBody.transform.localScale = new Vector3(-1, 1, 1);
                }
                else
                {
                    m_RigidBody.transform.localScale = new Vector3(1, 1, 1);
                }
            }


            m_RigidBody.transform.parent = m_CurrentHandGrabbing.m_CenterOfHand.transform;

            m_RigidBody.transform.localPosition = Vector3.zero;
            m_RigidBody.transform.localRotation = Quaternion.identity;

            //if (m_FlipGrabbableWhenHeld)
            //{
            if (m_CurrentHandGrabbing.m_PhysicsHand.m_Hi5Hand.HandType == Hand.LEFT)
            {
                //Vector3 flippedScale = m_RootOfGrabbable.localScale;
                //flippedScale.x = m_RootOfGrabbable.localScale.x * -1;
                //m_RootOfGrabbable.localScale = flippedScale;

                //m_RigidBody.transform.localScale = new Vector3(-1, 1, 1);

                if (m_OffsetGrabLocation != null)
                {
                    Vector3 offsetPosition = m_OffsetGrabLocation.localPosition;
                    Vector3 offsetRotation = m_OffsetGrabLocation.localRotation.eulerAngles;
                    m_RigidBody.transform.localPosition = new Vector3(-offsetPosition.x, offsetPosition.y, offsetPosition.z);
                    m_RigidBody.transform.localRotation = Quaternion.Euler(offsetRotation.x, -offsetRotation.y, -offsetRotation.z);
                }
            }
            else
            {
                //m_RigidBody.transform.localScale = new Vector3(1, 1, 1);

                if (m_OffsetGrabLocation != null)
                {
                    m_RigidBody.transform.localPosition = m_OffsetGrabLocation.localPosition;
                    m_RigidBody.transform.localRotation = m_OffsetGrabLocation.localRotation;
                }
            }
            //}

            ToggleRigidbodyGrabbed(true);
            TogglePhysicalColliders(true);

            m_IsGrabbed = true;
            m_CanBeGrabbed = false;
            m_PickUpTriggerCollider.enabled = false;

            Grabbed();
            m_CurrentHandGrabbing.GrabbableGrabbed();
        }

        public virtual void ReleaseGrabbable(PhysicsHandGrabber bindToHand)
        {
            m_CurrentHandGrabbing = bindToHand;

            //Debug.Log("RELEASING GRABBABLE");
            ToggleRigidbodyGrabbed(false);
            StartCoroutine(ToggleCollidersOn());

            //if (m_FlipGrabbable)
            //{
            //    if (m_CurrentHandGrabbing.m_PhysicsHand.m_Hi5Hand.handType == HI5.HandType.LEFT)
            //    {
            //        Vector3 flippedScale = m_RootOfGrabbable.localScale;
            //        flippedScale.x = m_RootOfGrabbable.localScale.x * -1;
            //        m_RootOfGrabbable.localScale = flippedScale;
            //    }
            //}

            m_RigidBody.transform.parent = m_OriginalParent;
            m_CurrentHandGrabbing.m_HandInteraction.ThrowReleasedObject(m_RigidBody, m_CurrentHandGrabbing);

            Released();
            m_CurrentHandGrabbing.GrabbableReleased();
        }

        protected virtual void ToggleRigidbodyGrabbed(bool _Grabbed)
        {
            if (_Grabbed)
            {
                if (m_EnableGravityWhenHeld)
                {
                    m_RigidBody.useGravity = true;
                    m_CurrentHandGrabbing.m_PhysicsHand.m_ForceHandPhysicsUpdate = true;
                }
                else
                {
                    m_RigidBody.useGravity = false;
                }

                PickupGrabbable();
            }
            else
            {
                DropGrabbable();

                m_RigidBody.useGravity = true;
                m_CurrentHandGrabbing.m_PhysicsHand.m_ForceHandPhysicsUpdate = false;
            }
        }

        /// <summary>
        /// Modify Rigidbody, components on Grabbable, and add config joint if needed
        /// </summary>
        private void PickupGrabbable()
        {
            // Pickup as Kinematic, otherwise use physics
            if (m_MakeGrabbableKinematicWhenGrabbed)
            {
                m_RigidBody.isKinematic = true;
            }
            else
            {
                m_CurrentHandGrabbingRigidBody = m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody;

                if (m_MakeGrabbableChildOfHandWhenGrabbed)
                {
                    m_SavedRigidBody = new RigidbodyState(m_RigidBody);
                    Destroy(m_RigidBody);

                    TransferComponentsToHand();
                }
                else
                {

                    // Added so that we attach Grabbed object to hand using Config Joint instead of just parenting
                    if (m_ConfigJoint == null)
                    {
                        m_StartedWithConfigJoint = false;

                        ConfigurableState configState = new ConfigurableState();
                        m_ConfigJoint = m_RigidBody.gameObject.AddComponent<ConfigurableJoint>();
                        configState.SetConfigurable(m_ConfigJoint);
                    }
                    else
                    {
                        m_StartedWithConfigJoint = true;
                        m_ConfigJoint.xMotion = ConfigurableJointMotion.Locked;
                        m_ConfigJoint.yMotion = ConfigurableJointMotion.Locked;
                        m_ConfigJoint.zMotion = ConfigurableJointMotion.Locked;
                        m_ConfigJoint.angularXMotion = ConfigurableJointMotion.Locked;
                        m_ConfigJoint.angularYMotion = ConfigurableJointMotion.Locked;
                        m_ConfigJoint.angularZMotion = ConfigurableJointMotion.Locked;
                    }

                    m_ConfigJoint.connectedBody = m_CurrentHandGrabbingRigidBody.m_RigidBody;

                }
            }
        }

        private void DropGrabbable()
        {
            if (m_MakeGrabbableKinematicWhenGrabbed)
            {
                m_RigidBody.useGravity = true;
                m_RigidBody.isKinematic = false;
            }
            else
            {
                if (m_MakeGrabbableChildOfHandWhenGrabbed)
                {
                    m_RigidBody = m_RigidBodyTransform.gameObject.AddComponent<Rigidbody>();
                    m_SavedRigidBody.SetRigidbody(m_RigidBody);

                    RetrieveComponentsFromHand();
                }
                else
                {
                    m_RigidBody.useGravity = true;

                    if (!m_StartedWithConfigJoint)
                    {
                        Destroy(m_ConfigJoint);
                    }
                    else
                    {
                        m_ConfigJoint.xMotion = ConfigurableJointMotion.Free;
                        m_ConfigJoint.yMotion = ConfigurableJointMotion.Free;
                        m_ConfigJoint.zMotion = ConfigurableJointMotion.Free;
                        m_ConfigJoint.angularXMotion = ConfigurableJointMotion.Free;
                        m_ConfigJoint.angularYMotion = ConfigurableJointMotion.Free;
                        m_ConfigJoint.angularZMotion = ConfigurableJointMotion.Free;
                        m_ConfigJoint.connectedBody = null;
                    }
                }

                m_CurrentHandGrabbingRigidBody = null;
            }
        }

        protected virtual IEnumerator ToggleCollidersOn()
        {
            yield return new WaitForSeconds(m_TimeUntilCollidable);
            TogglePhysicalColliders(false);
            m_CanBeGrabbed = true;
            m_IsGrabbed = false;
            m_PickUpTriggerCollider.enabled = true;
            m_CurrentHandGrabbing = null;
        }

        /// <summary>
        /// Originally, this was made to Disable/Enable colliders, now it changes layers of colliders
        /// </summary>
        /// <param name="_Grabbed"></param>
        protected void TogglePhysicalColliders(bool _Grabbed)
        {
            foreach (Collider c in m_PickUpPhysicalColliders)
            {
                if (_Grabbed)
                {
                    m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ReassignJointLayersName(true);
                    c.gameObject.layer = LayerMask.NameToLayer("GrabbableInHand");
                }
                else
                {
                    m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ReassignJointLayersName(false);
                    c.gameObject.layer = LayerMask.NameToLayer("Default");
                }
            }
        }

        protected virtual void Grabbed() { }

        protected virtual void Released()
        {
            // Added to fix issue when compound colliders are colliding with collidable and pistol is dropped,
            // the compound colliders wont call their colliders CollisionExit() which will make hands keep colliding.
            if (PhysicsInteractionManager.Instance.m_ResetHandCollisionsAfterReleasingGrabbable)
            {
                m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ClearAllCollisions();
            }
        }

        private void TransferComponentsToHand()
        {
            if (m_OnCollisionComponents != null && m_OnCollisionComponents.Count > 0)
            {
                m_OnCollisionComponentsOnRigidbody = new List<Component>();

                foreach (Component c in m_OnCollisionComponents)
                {
                    m_OnCollisionComponentsOnRigidbody.Add(CopyComponent(c, m_CurrentHandGrabbingRigidBody.gameObject));
                }

                for(int i = 0; i < m_OnCollisionComponents.Count; i++)
                {
                    if(m_OnCollisionComponents[i] != null)
                    {
                        Destroy(m_OnCollisionComponents[i]);
                    }
                }
            }
        }

        private void RetrieveComponentsFromHand()
        {
            if (m_OnCollisionComponentsOnRigidbody != null && m_OnCollisionComponentsOnRigidbody.Count > 0)
            {
                m_OnCollisionComponents = new List<Component>();

                foreach (Component c in m_OnCollisionComponentsOnRigidbody)
                {
                    m_OnCollisionComponents.Add(CopyComponent(c, gameObject));
                }

                for (int i = 0; i < m_OnCollisionComponentsOnRigidbody.Count; i++)
                {
                    if (m_OnCollisionComponentsOnRigidbody[i] != null)
                    {
                        Destroy(m_OnCollisionComponentsOnRigidbody[i]);
                    }
                }
            }
        }

        public Component CopyComponent(Component original, GameObject destination)
        {
            System.Type type = original.GetType();
            Component copy = destination.AddComponent(type);
            // Copied fields can be restricted with BindingFlags
            System.Reflection.FieldInfo[] fields = type.GetFields();
            foreach (System.Reflection.FieldInfo field in fields)
            {
                field.SetValue(copy, field.GetValue(original));
            }
            return copy;
        }

    }
}