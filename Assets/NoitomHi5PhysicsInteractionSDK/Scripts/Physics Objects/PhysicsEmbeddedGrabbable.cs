﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HI5.PhysicsInteraction
{
    public class PhysicsEmbeddedGrabbable : PhysicsGrabbable
    {
        public GameObject m_LeftHandMesh;
        public GameObject m_RightHandMesh;

        private GameObject m_ActiveMesh;

        public override void PickUpGrabbable(PhysicsHandGrabber bindToHand)
        {
            //base.PickUpGrabbable(bindToHand);

            Debug.Log("GRABBING EMBEDDED GRABBABLE: " + gameObject.name);

            m_CurrentHandGrabbing = bindToHand;

            TogglePhysicalColliders(true);

            m_IsGrabbed = true;
            m_CanBeGrabbed = false;
            m_PickUpTriggerCollider.enabled = false;

            HandleGrabbing();

            Grabbed();
            m_CurrentHandGrabbing.GrabbableGrabbed();
        }

        public override void ReleaseGrabbable(PhysicsHandGrabber bindToHand)
        {
            //base.ReleaseGrabbable(bindToHand);

            Debug.Log("RELEASING EMBEDDED GRABBABLE");

            m_CurrentHandGrabbing = bindToHand;

            HandleReleasing();

            StartCoroutine(ToggleCollidersOn());

            Released();
            m_CurrentHandGrabbing.GrabbableReleased();
        }

        protected virtual void HandleGrabbing()
        {
            m_CurrentHandGrabbing.m_PhysicsHand.TogglePhysicsHand(false);

            if(m_CurrentHandGrabbing.m_PhysicsHand.m_Hi5Hand.HandType == Hand.LEFT)
            {
                m_ActiveMesh = m_LeftHandMesh;
            }
            else
            {
                m_ActiveMesh = m_RightHandMesh;
            }

            m_ActiveMesh.SetActive(true);
        }

        protected virtual void HandleReleasing()
        {
            m_CurrentHandGrabbing.m_PhysicsHand.TogglePhysicsHand(true);

            m_ActiveMesh.SetActive(false);
        }

    }
}