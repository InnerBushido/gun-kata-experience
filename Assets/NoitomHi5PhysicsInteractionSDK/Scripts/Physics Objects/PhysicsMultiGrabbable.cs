﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class PhysicsMultiGrabbable : PhysicsGrabbable {
    
    public HandDominance m_HandDominance = HandDominance.RightHand;
    public bool m_AllowForMoreThanTwoHands = false;

    internal Transform m_RightHand;
    internal Transform m_LeftHand;

    internal HeldType m_CurrentHeldType = HeldType.NotGrabbed;
    internal int m_NumOfHandsGrabbing = 0;
    internal List<PhysicsHandGrabber> m_CurrentHandsGrabbing = new List<PhysicsHandGrabber>();

    public enum HandDominance
    {
        DoesntMatter,
        LeftHand,
        RightHand
    }

    internal enum HeldType
    {
        NotGrabbed,
        TwoHandGrabbed,         // Grabbed with two hands, dominant hand not specified
        MultiGrabbed,           // Grabbed with more than two, dominant hand not specified
        RightHandSingleGrabbed, // Grabbed with only Right Hand
        LeftHandSingleGrabbed,  // Grabbed with only Left Hand
        RightHandMultiGrabbed,  // Grabbed with two or more hands, right hand dominant
        LeftHandMultiGrabbed    // Grabbed with two or more hands, left hand dominant
    }

    protected override void Grabbed()
    {
        base.Grabbed();
    }

    //protected override void Released()
    //{
    //    if(m_NumOfHandsGrabbing < 0)
    //    {
    //        m_NumOfHandsGrabbing = 0;
    //        Debug.LogError("m_NumOfHandsGrabbing < 0, This shouldn't happen.");
    //    }

    //    base.Released();
    //}

    //public override void PickUpGrabbable(PhysicsHandGrabber bindToHand)
    //{
    //    m_NumOfHandsGrabbing++;
    //    m_CurrentHandsGrabbing.Add(bindToHand);
    //    AssignGrabbableType(bindToHand, true);

    //    if (m_NumOfHandsGrabbing == 1)
    //    {
    //        base.PickUpGrabbable(bindToHand);
    //    }

    //    if( m_AllowForMoreThanTwoHands ||
    //        (!m_AllowForMoreThanTwoHands && m_NumOfHandsGrabbing < 2))
    //    {
    //        m_CanBeGrabbed = true;
    //        m_PickUpTriggerCollider.enabled = true;
    //    }
    //}

    //public override void ReleaseGrabbable(PhysicsHandGrabber bindToHand)
    //{
    //    m_NumOfHandsGrabbing--;
    //    m_CurrentHandsGrabbing.Remove(bindToHand);
    //    AssignGrabbableType(bindToHand, false);

    //    if (m_NumOfHandsGrabbing <= 0)
    //    {
    //        base.ReleaseGrabbable(bindToHand);
    //    }

    //    m_CanBeGrabbed = true;
    //    m_PickUpTriggerCollider.enabled = true;
    //}

    private void AssignGrabbableType(PhysicsHandGrabber bindToHand, bool isGrabbing)
    {
        if (bindToHand.m_PhysicsHand.m_Hi5Hand.HandType == HI5.Hand.LEFT)
        {
            AssignCurrentHeldType(true);

            if (isGrabbing)
                m_LeftHand = bindToHand.m_PhysicsHand.m_CenterPivotOfHand;
            else
                m_LeftHand = null;
        }
        else
        {
            AssignCurrentHeldType(false);

            if (isGrabbing)
                m_RightHand = bindToHand.m_PhysicsHand.m_CenterPivotOfHand;
            else
                m_RightHand = null;
        }
    }

    private void AssignCurrentHeldType(bool isLeftHand)
    {
        if(m_NumOfHandsGrabbing == 0)
        {
            m_CurrentHeldType = HeldType.NotGrabbed;
        }
        else if(m_NumOfHandsGrabbing == 1)
        {
            if (isLeftHand)
            {
                m_CurrentHeldType = HeldType.LeftHandSingleGrabbed;
            }
            else if (!isLeftHand)
            {
                m_CurrentHeldType = HeldType.RightHandSingleGrabbed;
            }
        }
        else if (m_NumOfHandsGrabbing >= 2)
        {
            if (m_HandDominance == HandDominance.DoesntMatter)
            {
                if (m_NumOfHandsGrabbing == 2)
                {
                    m_CurrentHeldType = HeldType.TwoHandGrabbed;
                }
                else if (m_NumOfHandsGrabbing > 2)
                {
                    m_CurrentHeldType = HeldType.MultiGrabbed;
                }
            }
            else if (m_HandDominance == HandDominance.LeftHand)
            {
                m_CurrentHeldType = HeldType.LeftHandMultiGrabbed;
            }
            else if (m_HandDominance == HandDominance.RightHand)
            {
                m_CurrentHeldType = HeldType.RightHandMultiGrabbed;
            }
        }
        else
        {
            Debug.LogError("CHECK HANDS ON MUTLI GRABBABLE, VAL = " + m_NumOfHandsGrabbing);
            m_CurrentHeldType = HeldType.MultiGrabbed;
        }
    }

}
