﻿using UnityEngine;

// Deprecated
namespace HI5.PhysicsInteraction
{
    /// <summary>
    /// Used To Spawn and calculate the AABB of the Hand collider every frame using all colliders on the hand
    /// </summary>
    public class HandAABBCollider : MonoBehaviour
    {
        public PhysicsRigidBody m_PhysicsRigidBody;

        BoxCollider m_HandCollider;
        Bounds m_HandBounds = new Bounds();

        public Collider[] m_CollidersToUseForBounds;
        public Collider[] m_CollidersInHand;
        Transform m_OriginalParent;

        void Awake()
        {
            m_HandCollider = GetComponentInChildren<BoxCollider>();

            //m_CollidersInHand = m_PhysicsRigidBody.gameObject.GetComponentsInChildren<Collider>(false);
            //DisableAllHandColliders();

            m_CollidersInHand = m_CollidersToUseForBounds;

            m_OriginalParent = transform.parent;
            transform.parent = null;
            //transform.parent = m_PhysicsRigidBody.transform;
        }

        void FixedUpdate()
        {
            FindBoundsOfAllColliders();
            SetBoundsOfHandCollider();
        }

        void FindBoundsOfAllColliders()
        {
            //m_HandBounds.center = m_PhysicsRigidBody.transform.position;
            m_HandBounds.center = m_CollidersInHand[0].transform.position;
            m_HandBounds.size = Vector3.zero;

            foreach (Collider c in m_CollidersInHand)
            {
                m_HandBounds.Encapsulate(c.bounds);
            }
        }

        void SetBoundsOfHandCollider()
        {
            m_HandCollider.transform.position = m_HandBounds.center;
            m_HandCollider.size = m_HandBounds.size;
        }

        void DisableAllHandColliders()
        {
            foreach (Collider c in m_CollidersInHand)
            {
                c.enabled = false;
            }
        }
    }
}
