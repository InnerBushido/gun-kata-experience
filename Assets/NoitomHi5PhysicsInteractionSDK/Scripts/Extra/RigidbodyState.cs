using UnityEngine;

namespace HI5.PhysicsInteraction
{
    public class RigidbodyState : ScriptableObject
    {

        public Vector3 velocity;
        public Vector3 angularVelocity;
        public float drag;
        public float angularDrag;
        public float mass;
        public bool useGravity;
        public bool isKinematic;
        public bool freezeRotation;
        public Vector3 centerOfMass;
        public Vector3 worldCenterOfMass;
        public Quaternion inertiaTensorRotation;
        public Vector3 inertiaTensor;
        public bool detectCollisions;
        public bool useConeFriction;
        public Vector3 position;
        public Quaternion rotation;
        public RigidbodyInterpolation interpolation;
        public int solverIterationCount;
        public float sleepVelocity;
        public float sleepAngularVelocity;
        public float maxAngularVelocity;

        public RigidbodyState(Rigidbody rb)
        {
            //velocity = rb.velocity;
            //angularVelocity = rb.angularVelocity;
            drag = rb.drag;
            angularDrag = rb.angularDrag;
            mass = rb.mass;
            useGravity = rb.useGravity;
            isKinematic = rb.isKinematic;
            //freezeRotation = rb.freezeRotation;
            //centerOfMass = rb.centerOfMass;
            //worldCenterOfMass = rb.worldCenterOfMass;
            //inertiaTensorRotation = rb.inertiaTensorRotation;
            //      inertiaTensor = rb.inertiaTensor;
            //      detectCollisions = rb.detectCollisions;
            //      useConeFriction = rb.useConeFriction;
            //      position = rb.position;
            //      rotation = rb.rotation;
            //      interpolation = rb.interpolation;
            //      solverIterationCount = rb.solverIterations;
            //      sleepVelocity = rb.sleepVelocity;
            //      sleepAngularVelocity = rb.sleepAngularVelocity;
            //      maxAngularVelocity = rb.maxAngularVelocity;
        }

        // even though this isn't passing by reference, it still works due to some 
        // oddity in Unity. WHO KNOWS. Using ref gives an error.
        public void SetRigidbody(Rigidbody rb)
        {
            //rb.velocity = velocity;
            //rb.angularVelocity = angularVelocity;
            rb.drag = drag;
            rb.angularDrag = angularDrag;
            rb.mass = mass;
            rb.useGravity = useGravity;
            rb.isKinematic = isKinematic;
            //rb.freezeRotation = freezeRotation;
            //rb.centerOfMass = centerOfMass;
            // read-only
            ////rb.worldCenterOfMass = worldCenterOfMass;
            //rb.inertiaTensorRotation = inertiaTensorRotation;
            //rb.inertiaTensor = inertiaTensor;
            //rb.detectCollisions = detectCollisions;
            //rb.useConeFriction = useConeFriction;
            //rb.position = position;
            //rb.rotation = rotation;
            //rb.interpolation = interpolation;
            //rb.solverIterations = solverIterationCount;
            //rb.sleepVelocity = sleepVelocity;
            //rb.sleepAngularVelocity = sleepAngularVelocity;
            //rb.maxAngularVelocity = maxAngularVelocity;
        }
    }
}