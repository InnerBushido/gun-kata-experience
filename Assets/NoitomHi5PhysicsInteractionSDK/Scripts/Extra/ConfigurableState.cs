﻿using UnityEngine;

namespace HI5.PhysicsInteraction
{
    public class ConfigurableState : ScriptableObject
    {
        // Defaults set to locked position/rotaiton
        public Rigidbody connectedBody = null;
        public Vector3 axis = new Vector3(1, 0, 0);
        public bool autoconfigureConnectedAnchor = true;
        public Vector3 connectedAnchor = new Vector3();
        public Vector3 secondaryAxis = new Vector3(0, 1, 0);
        public ConfigurableJointMotion xMotion = ConfigurableJointMotion.Locked;
        public ConfigurableJointMotion yMotion = ConfigurableJointMotion.Locked;
        public ConfigurableJointMotion zMotion = ConfigurableJointMotion.Locked;
        public ConfigurableJointMotion angularXMotion = ConfigurableJointMotion.Locked;
        public ConfigurableJointMotion angularYMotion = ConfigurableJointMotion.Locked;
        public ConfigurableJointMotion angularZMotion = ConfigurableJointMotion.Locked;

        public SoftJointLimitSpring linearLimitSpring = new SoftJointLimitSpring();
        public SoftJointLimit linearLimit = new SoftJointLimit();

        public SoftJointLimitSpring angularXLimitSpring = new SoftJointLimitSpring();
        public SoftJointLimitSpring angularYZLimitSpring = new SoftJointLimitSpring();

        public Vector3 targetPosition = new Vector3();
        public Vector3 targetVelocity = new Vector3();
        public Quaternion targetRotation = new Quaternion();
        public Vector3 targetAngularVelocity = new Vector3();

        public JointDrive xDrive = new JointDrive();
        public JointDrive yDrive = new JointDrive();
        public JointDrive zDrive = new JointDrive();
        public JointDrive angularXDrive = new JointDrive();
        public JointDrive angularYZDrive = new JointDrive();

        public JointProjectionMode projectionMode = new JointProjectionMode();
        public float projectionDistance = 0;
        public float projectionAngle = 0;

        public bool configureInWorldSpace = false;
        public bool swapBodies = false;
        public float breakForce = Mathf.Infinity;
        public float breakTorque = Mathf.Infinity;
        public bool enableCollision = false;
        public bool enablePreprocessing = true;

        public ConfigurableState()
        {
            linearLimitSpring.spring = 8000;
            linearLimitSpring.damper = 0.5f;

            linearLimit.limit = 0;
            linearLimit.bounciness = 0;
            linearLimit.contactDistance = 0;

            angularXLimitSpring.spring = 8000;
            angularXLimitSpring.damper = 0.5f;

            angularYZLimitSpring.spring = 8000;
            angularYZLimitSpring.damper = 0.5f;

            xDrive.positionSpring = 2000;
            xDrive.positionDamper = 0.5f;
            xDrive.maximumForce = 0;

            yDrive.positionSpring = 2000;
            yDrive.positionDamper = 0.5f;
            yDrive.maximumForce = 0;

            zDrive.positionSpring = 2000;
            zDrive.positionDamper = 0.5f;
            zDrive.maximumForce = 0;

            angularXDrive.positionSpring = 2000;
            angularXDrive.positionDamper = 0.5f;
            angularXDrive.maximumForce = 0;

            angularYZDrive.positionSpring = 2000;
            angularYZDrive.positionDamper = 0.5f;
            angularYZDrive.maximumForce = 0;

            projectionMode = JointProjectionMode.PositionAndRotation;

        }

        public ConfigurableState(ConfigurableJoint joint)
        {
            connectedBody = joint.connectedBody;
            axis = joint.axis;
            autoconfigureConnectedAnchor = joint.autoConfigureConnectedAnchor;
            connectedAnchor = joint.connectedAnchor;
            secondaryAxis = joint.secondaryAxis;
            xMotion = joint.xMotion;
            yMotion = joint.yMotion;
            zMotion = joint.zMotion;
            angularXMotion = joint.angularXMotion;
            angularYMotion = joint.angularYMotion;
            angularZMotion = joint.angularZMotion;

            linearLimitSpring = joint.linearLimitSpring;
            linearLimit = joint.linearLimit;

            angularXLimitSpring = joint.angularXLimitSpring;
            angularYZLimitSpring = joint.angularYZLimitSpring;


            targetPosition = joint.targetPosition;
            targetVelocity = joint.targetVelocity;
            targetRotation = joint.targetRotation;
            targetAngularVelocity = joint.targetAngularVelocity;

            xDrive = joint.xDrive;
            yDrive = joint.yDrive;
            zDrive = joint.zDrive;
            angularXDrive = joint.angularXDrive;
            angularYZDrive = joint.angularYZDrive;

            projectionMode = joint.projectionMode;
            projectionDistance = joint.projectionDistance;
            projectionAngle = joint.projectionAngle;
            configureInWorldSpace = joint.configuredInWorldSpace;
            swapBodies = joint.swapBodies;
            breakForce = joint.breakForce;
            breakTorque = joint.breakTorque;
            enableCollision = joint.enableCollision;
            enablePreprocessing = joint.enablePreprocessing;
        }

        public void SetConfigurable(ConfigurableJoint joint)
        {
            joint.connectedBody = connectedBody;
            joint.axis = axis;
            joint.autoConfigureConnectedAnchor = autoconfigureConnectedAnchor;
            joint.connectedAnchor = connectedAnchor;
            joint.secondaryAxis = secondaryAxis;
            joint.xMotion = xMotion;
            joint.yMotion = yMotion;
            joint.zMotion = zMotion;
            joint.angularXMotion = angularXMotion;
            joint.angularYMotion = angularYMotion;
            joint.angularZMotion = angularZMotion;
            joint.linearLimitSpring = linearLimitSpring;
            joint.linearLimit = linearLimit;
            joint.angularXLimitSpring = angularXLimitSpring;
            joint.angularYZLimitSpring = angularYZLimitSpring;
            joint.targetPosition = targetPosition;
            joint.targetVelocity = targetVelocity;
            joint.targetRotation = targetRotation;
            joint.targetAngularVelocity = targetAngularVelocity;
            joint.xDrive = xDrive;
            joint.yDrive = yDrive;
            joint.zDrive = zDrive;
            joint.angularXDrive = angularXDrive;
            joint.angularYZDrive = angularYZDrive;
            joint.projectionMode = projectionMode;
            joint.projectionDistance = projectionDistance;
            joint.projectionAngle = projectionAngle;
            joint.configuredInWorldSpace = configureInWorldSpace;
            joint.swapBodies = swapBodies;
            joint.breakForce = breakForce;
            joint.breakTorque = breakTorque;
            joint.enableCollision = enableCollision;
            joint.enablePreprocessing = enablePreprocessing;
        }

    }
}
