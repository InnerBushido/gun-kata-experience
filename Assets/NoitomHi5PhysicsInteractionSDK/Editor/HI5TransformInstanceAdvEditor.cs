﻿using UnityEngine;
using UnityEditor;

namespace HI5.PhysicsInteraction
{
    [CustomEditor(typeof(HI5TransformInstanceAdv))]
    public class HI5TransformInstanceAdvEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            HI5_TransformInstance targetScript = (HI5_TransformInstance)target;

            if (targetScript.HandType == Hand.LEFT)
            {
                if (GUILayout.Button("Load LEFT Hand References"))
                {
                    Debug.Log("[HI5TransformInstance] - LOAD Left hand Transform references into the bones list!");
                    targetScript.AutoBindBones(Hand.LEFT);
                    EditorUtility.SetDirty(targetScript);
                }
            }

            if (targetScript.HandType == Hand.RIGHT)
            {
                if (GUILayout.Button("Load RIGHT Hand References"))
                {
                    Debug.Log("[HI5TransformInstance] - LOAD Right hand Transform references into the bones list!");
                    targetScript.AutoBindBones(Hand.RIGHT);
                    EditorUtility.SetDirty(targetScript);
                }
            }
        }

    }
}