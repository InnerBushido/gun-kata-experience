﻿using UnityEngine;
using UnityEditor;

namespace HI5.PhysicsInteraction
{
    [CustomEditor(typeof(PhysicsHand))]
    public class HI5TransformHandPhysicsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            PhysicsHand targetScript = (PhysicsHand)target;

            if (targetScript.m_Hi5Hand != null && targetScript.m_Hi5Hand.HandType == Hand.LEFT)
            {
                if (GUILayout.Button("Load LEFT Hand References"))
                {
                    Debug.Log("[HI5TransformInstance] - LOAD Left hand Transform references into the bones list!");
                    //HI5Helper.AutoBindBones(targetScript.transform, targetScript.m_HandBones, "Human_", HandType.LEFT);
                    HI5_Manager.BindBones(targetScript.transform, targetScript.m_HandBones, "Human_", Hand.LEFT);
                    EditorUtility.SetDirty(targetScript);
                }
            }

            if (targetScript.m_Hi5Hand != null && targetScript.m_Hi5Hand.HandType == Hand.RIGHT)
            {
                if (GUILayout.Button("Load RIGHT Hand References"))
                {
                    Debug.Log("[HI5TransformInstance] - LOAD Right hand Transform references into the bones list!");
                    //HI5Helper.AutoBindBones(targetScript.transform, targetScript.m_HandBones, "Human_", HandType.RIGHT);
                    HI5_Manager.BindBones(targetScript.transform, targetScript.m_HandBones, "Human_", Hand.RIGHT);
                    EditorUtility.SetDirty(targetScript);
                }
            }
        }
    }
}
