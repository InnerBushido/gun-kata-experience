﻿using UnityEngine;
using System.Collections;
using ShatterToolkit;

public class ScissorCutting : MonoBehaviour {

    public AudioClip hitSound;
    public TrailRenderer trailRenderer;

    protected Vector3 start, end, endRotation;
    protected bool started = false;

    Vector3 newpos, oldpos, velocity;
    public bool swordSwinging = false;

    void Start()
    {
        oldpos = transform.position;

        if (trailRenderer != null)
        {
            trailRenderer.enabled = false;
        }
    }

    void Update()
    {        
        newpos = transform.position;
        var media = (newpos - oldpos);
        velocity = media / Time.deltaTime;

        //Debug.Log("MAGNITUDE IS: " + velocity.magnitude);

        if (velocity.magnitude > 3 && !swordSwinging)
        {
            GetComponent<AudioSource>().Play();
            swordSwinging = true;
            if (trailRenderer != null)
            {
                trailRenderer.enabled = true;
            }
        }
        else if (velocity.magnitude < 1  && swordSwinging)
        {
            swordSwinging = false;
            if (trailRenderer != null)
            {
                trailRenderer.enabled = false;
            }
        }

        oldpos = newpos;
        newpos = transform.position;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ShatterTool>() != null && swordSwinging)
        {
            start = transform.position;
            started = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<ShatterTool>() != null)
        {
            if (started)
            {
                end = transform.position;
                Quaternion tempRotation = transform.rotation;
                endRotation = tempRotation * Vector3.forward;

                Vector3 line = end - start;

                Plane splitPlane = new Plane(Vector3.Normalize(Vector3.Cross(line, endRotation)), end);

                other.SendMessage("Split", new Plane[] { splitPlane }, SendMessageOptions.DontRequireReceiver);

                GetComponent<AudioSource>().PlayOneShot(hitSound);

            }

            started = false;
        }
    }

}
