﻿using UnityEngine;
using System.Collections;
using ShatterToolkit;

public class CheckTatami : MonoBehaviour {

    public DeleteTatami deleter;
    public GameObject tatamiPrefab;
    public Transform spawnTatamiHere;

    bool isColliding = true;
    Coroutine currentCoroutine;

    void FixedUpdate()
    {
        if (!isColliding)
        {
            //Debug.Log("NOT COLLIDING");

            if (currentCoroutine == null)
            {
                currentCoroutine = StartCoroutine(ResetTatami());
            }
        }
        else if(currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
            currentCoroutine = null;
        }

        isColliding = false;
    }


    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<ShatterTool>() != null)
        {
            isColliding = true;
            //Debug.Log("COLLIDING");
        }
    }

    IEnumerator ResetTatami()
    {
        yield return new WaitForSeconds(1);
        Debug.Log("RESETTING TATAMI");
        deleter.DeleteObjects();
        yield return new WaitForSeconds(0.1f);
        Instantiate(tatamiPrefab, spawnTatamiHere.position, Quaternion.identity);
    }

}
