﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateHandOnSword : MonoBehaviour
{
    public Transform m_SwordHandle;
    public Transform m_SwordHandleVerticalPlane;
    public Transform m_SwordHandleHorizontalPlane;
    public Transform m_HandPosition;
    public Vector3 m_MoveInAxis = Vector3.right;

    public float m_MaximumVal;
    public float m_MinimumVal;

    private Plane m_VerticalPlane;
    private Plane m_HorizontalPlane;
    private Vector3 m_VerticalPlaneNormal;
    private Vector3 m_HorizontalPlaneNormal;
    private Vector3 m_SolvedVerticalPosition;
    private Vector3 m_SolvedHorizontalPosition;

    private Ray m_HandToSwordRay = new Ray();
    RaycastHit hit;
    float hitDistance;

    Vector3 position;

    private void Update()
    {
        PositionSwordVerticalPlane();
        PositionSwordHorizontalPlane();

        FigureOutIntersectionOnSwordVerticalPlane();
        FigureOutIntersectionOnSwordHorizontalPlane();

        transform.position = m_SolvedHorizontalPosition;

        LimitValue();
    }

    private void LimitValue()
    {
        if(m_MoveInAxis == Vector3.right)
        {
            if (transform.localPosition.x > m_MaximumVal)
            {
                position = transform.localPosition;
                position.x = m_MaximumVal;
                transform.localPosition = position;
            }
            else if(transform.localPosition.x < m_MinimumVal)
            {
                position = transform.localPosition;
                position.x = m_MinimumVal;
                transform.localPosition = position;
            }
        }
        else if (m_MoveInAxis == Vector3.forward)
        {
            if (transform.localPosition.z > m_MaximumVal)
            {
                position = transform.localPosition;
                position.z = m_MaximumVal;
                transform.localPosition = position;
            }
            else if (transform.localPosition.z < m_MinimumVal)
            {
                position = transform.localPosition;
                position.z = m_MinimumVal;
                transform.localPosition = position;
            }
        }
    }

    private void PositionSwordVerticalPlane()
    {
        m_VerticalPlane = new Plane(m_SwordHandle.forward, m_SwordHandle.position);
        m_VerticalPlaneNormal = m_VerticalPlane.normal;
        m_VerticalPlaneNormal.Normalize();
        
        m_SwordHandleVerticalPlane.position = m_SwordHandle.position;
        m_SwordHandleVerticalPlane.LookAt(m_SwordHandle.position - m_VerticalPlaneNormal, m_SwordHandle.up);
    }

    private void PositionSwordHorizontalPlane()
    {
        m_HorizontalPlane = new Plane(m_SwordHandle.up, m_SwordHandle.position);
        m_HorizontalPlaneNormal = m_HorizontalPlane.normal;
        m_HorizontalPlaneNormal.Normalize();

        m_SwordHandleHorizontalPlane.position = m_SwordHandle.position;
        m_SwordHandleHorizontalPlane.LookAt(m_SwordHandle.position - m_HorizontalPlaneNormal, m_SwordHandle.up);
    }

    private void FigureOutIntersectionOnSwordVerticalPlane()
    {
        m_HandToSwordRay.origin = m_HandPosition.position;
        m_HandToSwordRay.direction = -m_VerticalPlane.normal;
        Debug.DrawRay(m_HandToSwordRay.origin, m_HandToSwordRay.direction, Color.yellow);

        if (m_VerticalPlane.Raycast(m_HandToSwordRay, out hitDistance))
        {
            m_SolvedVerticalPosition = m_HandToSwordRay.origin + (m_HandToSwordRay.direction.normalized * hitDistance);
            Debug.DrawLine(m_SwordHandle.position, m_SolvedVerticalPosition, Color.red);
        }
        else
        {
            m_HandToSwordRay.direction = -m_HandToSwordRay.direction;
            if (m_VerticalPlane.Raycast(m_HandToSwordRay, out hitDistance))
            {
                m_SolvedVerticalPosition = m_HandToSwordRay.origin + (m_HandToSwordRay.direction.normalized * hitDistance);
                Debug.DrawLine(m_SwordHandle.position, m_SolvedVerticalPosition, Color.red);
            }
        }
    }

    private void FigureOutIntersectionOnSwordHorizontalPlane()
    {
        m_HandToSwordRay.origin = m_SolvedVerticalPosition;
        m_HandToSwordRay.direction = -m_HorizontalPlane.normal;
        Debug.DrawRay(m_HandToSwordRay.origin, m_HandToSwordRay.direction, Color.blue);

        if (m_HorizontalPlane.Raycast(m_HandToSwordRay, out hitDistance))
        {
            m_SolvedHorizontalPosition = m_HandToSwordRay.origin + (m_HandToSwordRay.direction.normalized * hitDistance);
            Debug.DrawLine(m_SwordHandle.position, m_SolvedHorizontalPosition, Color.red);
        }
        else
        {
            m_HandToSwordRay.direction = -m_HandToSwordRay.direction;
            if (m_HorizontalPlane.Raycast(m_HandToSwordRay, out hitDistance))
            {
                m_SolvedHorizontalPosition = m_HandToSwordRay.origin + (m_HandToSwordRay.direction.normalized * hitDistance);
                Debug.DrawLine(m_SwordHandle.position, m_SolvedHorizontalPosition, Color.red);
            }
        }
    }

}
