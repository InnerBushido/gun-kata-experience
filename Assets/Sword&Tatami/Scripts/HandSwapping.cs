﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class HandSwapping : MonoBehaviour
{
    public Transform m_CurrentIKHand;
    //public Transform m_SwordIKHand;
    public VRIK m_VRIK;
    public bool m_IsRightArm = true;

    //internal HandOnSword m_HandState = HandOnSword.OffSword;
    //internal enum HandOnSword
    //{
    //    OffSword,
    //    OnSword
    //}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            IKSolverVR.Arm arm;

            if (m_IsRightArm)
            {
                arm = m_VRIK.solver.rightArm;
            }
            else
            {
                arm = m_VRIK.solver.leftArm;
            }

            arm.bendGoalWeight = arm.bendGoalWeight >= 1 ? 0 : 1;
            arm.shoulderRotationWeight = arm.shoulderRotationWeight >= 1 ? 0 : 1;
        }
    }


    public void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.name == "HandTrigger")
        {
            SwordCutting sword = collider.gameObject.GetComponentInParent<SwordCutting>();

            if (sword != null)
            {
                if (m_IsRightArm)
                {
                    m_VRIK.solver.rightArm.target = sword.rightHandPosition;
                    sword.m_RightHandState = SwordCutting.HandOnSword.OnSword;
                }
                else
                {
                    m_VRIK.solver.leftArm.target = sword.leftHandPosition;
                    sword.m_LeftHandState = SwordCutting.HandOnSword.OnSword;
                }
            }
        }
    }

    public void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name == "HandTrigger")
        {
            SwordCutting sword = collider.gameObject.GetComponentInParent<SwordCutting>();

            if (sword != null)
            {
                if (m_IsRightArm)
                {
                    m_VRIK.solver.rightArm.target = m_CurrentIKHand;
                    sword.m_RightHandState = SwordCutting.HandOnSword.OffSword;
                }
                else
                {
                    m_VRIK.solver.leftArm.target = m_CurrentIKHand;
                    sword.m_LeftHandState = SwordCutting.HandOnSword.OffSword;
                }
            }
        }
    }

    public void VibrateHand()
    {
        if (m_IsRightArm)
        {
            HI5.HI5_Manager.RightHandVibrate(250);
        }
        else
        {
            HI5.HI5_Manager.LeftHandVibrate(250);
        }
    }
}
