﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5;
using Valve.VR;

public class FollowTrackerIK : MonoBehaviour
{
    public Hand m_HandType = Hand.RIGHT;

    SteamVR_Events.Action newPosesAction;

    private void Awake()
    {
        newPosesAction = SteamVR_Events.NewPosesAction(OnNewPoses);
    }

    void OnEnable()
    {
        newPosesAction.enabled = true;
    }

    void OnDisable()
    {
        newPosesAction.enabled = false;
    }

    private void OnNewPoses(TrackedDevicePose_t[] poses)
    {
        int index = m_HandType == Hand.LEFT ? HI5_BindInfoManager.LeftID : HI5_BindInfoManager.RightID;

        if (index == -1)
            return;

        if (poses.Length <= index)
            return;

        if (!poses[index].bDeviceIsConnected)
            return;

        if (!poses[index].bPoseIsValid)
            return;

        var pose = new SteamVR_Utils.RigidTransform(poses[index].mDeviceToAbsoluteTracking);

        transform.localPosition = pose.pos;
        transform.localRotation = pose.rot;
    }

}
