﻿using UnityEngine;
using System.Collections;
using ShatterToolkit;
using System.Collections.Generic;

public class DeleteTatami : MonoBehaviour {

    List<GameObject> objToDelete = new List<GameObject>();

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ShatterTool>() != null)
        {
            if (!objToDelete.Contains(other.gameObject))
            {
                objToDelete.Add(other.gameObject);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<ShatterTool>() != null)
        {
            if (objToDelete.Contains(other.gameObject))
            {
                objToDelete.Remove(other.gameObject);
            }
        }
    }

    public void DeleteObjects()
    {
        foreach (GameObject obj in objToDelete)
        {
            Destroy(obj);
        }

        objToDelete.Clear();
    }
}
