﻿using UnityEngine;
using System.Collections;
using ShatterToolkit;
using Valve.VR;

public class SwordCutting : MonoBehaviour {

    public AudioClip hitSound;
    public TrailRenderer trailRenderer;

    //public SteamVR_TrackedObject trackedController;
    public SteamVR_Behaviour_Pose trackedController;

    public HandSwapping leftHand;
    public HandSwapping rightHand;
    public Transform leftHandPosition;
    public Transform rightHandPosition;

    public SteamVR_Action_Vibration hapticAction = SteamVR_Input.GetAction<SteamVR_Action_Vibration>("Haptic");

    protected Vector3 start, end, endRotation;
    protected bool started = false;

    Vector3 newpos, oldpos, velocity;
    public bool swordSwinging = false;
    
    private SteamVR_Behaviour_Pose trackedDevice;

    internal HandOnSword m_RightHandState = HandOnSword.OffSword;
    internal HandOnSword m_LeftHandState = HandOnSword.OffSword;

    internal enum HandOnSword
    {
        OffSword,
        OnSword
    }

    void Start()
    {
        oldpos = transform.position;

        if (trailRenderer != null)
        {
            trailRenderer.enabled = false;
        }
                
        if (trackedController != null)
        {
            ////var deviceIndex = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost);
            //var index = (int)trackedController.index;

            //if (index != -1)
            //{
            //    trackedDevice = trackedController.GetComponent<SteamVR_Behaviour_Pose>();// SteamVR_Controller.Input(index);
            //}
            trackedDevice = trackedController.GetComponent<SteamVR_Behaviour_Pose>();
        }
    }

    void Update()
    {        
        newpos = transform.position;
        var media = (newpos - oldpos);
        velocity = media / Time.deltaTime;

        //Debug.Log("MAGNITUDE IS: " + velocity.magnitude);

        if (velocity.magnitude > 2 && !swordSwinging)
        {
            GetComponent<AudioSource>().Play();
            swordSwinging = true;
            //CheckHandHaptics();
            if (trailRenderer != null)
            {
                trailRenderer.enabled = true;
            }
        }
        else if (velocity.magnitude < 0.5f  && swordSwinging)
        {
            swordSwinging = false;
            if (trailRenderer != null)
            {
                trailRenderer.enabled = false;
            }
        }

        oldpos = newpos;
        newpos = transform.position;
    }

    void CheckHandHaptics()
    {
        if (leftHand && m_LeftHandState == HandOnSword.OnSword)
        {
            leftHand.VibrateHand();
        }

        if (rightHand && m_RightHandState == HandOnSword.OnSword)
        {
            rightHand.VibrateHand();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ShatterTool>() != null && swordSwinging)
        {
            start = transform.position;
            started = true;
                        
            if (trackedDevice != null)
            {
                StartCoroutine(HapticPulse());
            }
        }
    }

    private IEnumerator HapticPulse()
    {
        float waitTime = 0.02f;
        ushort strength = ushort.MaxValue;

        for(int i = 0; i < 8; i++)
        {
            yield return new WaitForSeconds(waitTime);
            //trackedDevice.TriggerHapticPulse(strength);
            TriggerHapticPulse(strength);
        }

    }

    public void TriggerHapticPulse(ushort microSecondsDuration)
    {
        float seconds = (float)microSecondsDuration / 1000000f;
        hapticAction.Execute(0, seconds, 1f / seconds, 1, trackedDevice.inputSource);
    }

    private IEnumerator ArrowReleaseHaptics()
    {
        yield return new WaitForSeconds(0.05f);

        //trackedDevice.TriggerHapticPulse(15000);
        TriggerHapticPulse(15000);
        yield return new WaitForSeconds(0.05f);

        //trackedDevice.TriggerHapticPulse(8000);
        TriggerHapticPulse(8000);
        yield return new WaitForSeconds(0.05f);

        //trackedDevice.TriggerHapticPulse(5000);
        TriggerHapticPulse(5000);
        yield return new WaitForSeconds(0.05f);

        //trackedDevice.TriggerHapticPulse(3000);
        TriggerHapticPulse(3000);
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<ShatterTool>() != null)
        {
            if (started)
            {
                end = transform.position;
                Quaternion tempRotation = transform.rotation;
                endRotation = tempRotation * Vector3.forward;

                Vector3 line = end - start;

                Plane splitPlane = new Plane(Vector3.Normalize(Vector3.Cross(line, endRotation)), end);

                other.SendMessage("Split", new Plane[] { splitPlane }, SendMessageOptions.DontRequireReceiver);

                GetComponent<AudioSource>().PlayOneShot(hitSound);

                if (trackedDevice != null)
                {
                    //StartCoroutine(HapticPulse());
                }

            }

            started = false;
        }
    }

}
