﻿using UnityEngine;
using System.Collections;
using Valve.VR;

public class MultipleCamerasSwitcher : MonoBehaviour {
	public KeyCode SwitchKey;
	public enum CamModes { HMD, HMD_Smoothed, ThirdPerson, ThirdPersonWithOverlay }
	public CamModes ActiveMode;

	public SteamVR_Camera ViveGameView;
	public Camera SmoothedCamera;
	public Camera ThirdPersonCamera;
	public Rect HMDOverlayRect;

	private int CurrentIndex;

	// Use this for initialization
	void Start () {
		// reset and set initial values
		//ViveGameView.enabled = false;
		SmoothedCamera.gameObject.SetActive(false);
		ThirdPersonCamera.gameObject.SetActive(false);
		SmoothedCamera.gameObject.SetActive(true);
		SmoothedCamera.rect = new Rect(0,0,1,1);
	
        ActiveMode = CamModes.HMD_Smoothed;
		SetCameraMode(ActiveMode, true);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (SwitchKey)) {
			//turn off first
			SetCameraMode(ActiveMode, false);

			//then get next mode and turn on
			int q = (int)ActiveMode;
			if (q < System.Enum.GetValues(typeof(CamModes)).Length-1){
				ActiveMode = (CamModes)q+1;
			} else {
				ActiveMode = (CamModes)0;
			}

			SetCameraMode(ActiveMode, true);

		}
	
	}

	void SetCameraMode(CamModes mode, bool status){
		switch (mode) {
		case CamModes.HMD:
			ViveGameView.enabled = status;
			break;
		case CamModes.HMD_Smoothed:
			SmoothedCamera.gameObject.SetActive(status);
			break;
		case CamModes.ThirdPerson:
			ThirdPersonCamera.gameObject.SetActive(status);
			break;
		case CamModes.ThirdPersonWithOverlay:
			ThirdPersonCamera.gameObject.SetActive(status);
			SmoothedCamera.gameObject.SetActive(status);
			if (status) {
				SmoothedCamera.rect = HMDOverlayRect;
			} else {
				SmoothedCamera.rect = new Rect(0,0,1,1);
			}
			break;
		}
	}
}
