﻿using UnityEngine;
using System.Collections;

public class LinkedVRCamera : MonoBehaviour {
	// What transform to chase:
	public Transform target;
	
	// Debug keyboard controls:
	public KeyCode modifier;
	public KeyCode incSmooth;
	public KeyCode decSmooth;

	public float CamSmoothingValue;

	private Vector3 moveVel;
	private Vector3 rotVel;
	
	void Awake() {
		moveVel = Vector3.zero;
		rotVel = Vector3.zero;
	}
	
	void Update () {
		if (modifier == KeyCode.None || Input.GetKey (modifier)) {	
			if (Input.GetKey (incSmooth)) {
				CamSmoothingValue += 0.001f;
			}
			if (Input.GetKey (decSmooth)) {
				CamSmoothingValue -= 0.001f;
				if (CamSmoothingValue < 0) CamSmoothingValue = 0;
				if (CamSmoothingValue < 0) CamSmoothingValue = 0;
			}
		}
		
		float deltaTime = Time.fixedDeltaTime;

        transform.position = Vector3.SmoothDamp(transform.position, target.position, ref moveVel, CamSmoothingValue, Mathf.Infinity, deltaTime);
        //transform.position = target.position;

        Vector3 eulerAngles = this.transform.rotation.eulerAngles;
        Vector3 targetEulerAngles = target.eulerAngles;
        eulerAngles.x = Mathf.SmoothDampAngle(eulerAngles.x, targetEulerAngles.x, ref rotVel.x, CamSmoothingValue, Mathf.Infinity, deltaTime);
        eulerAngles.y = Mathf.SmoothDampAngle(eulerAngles.y, targetEulerAngles.y, ref rotVel.y, CamSmoothingValue, Mathf.Infinity, deltaTime);
        eulerAngles.z = Mathf.SmoothDampAngle(eulerAngles.z, targetEulerAngles.z, ref rotVel.z, CamSmoothingValue, Mathf.Infinity, deltaTime);
        this.transform.rotation = Quaternion.Euler(eulerAngles);
        //transform.rotation = target.rotation;
    }
}
