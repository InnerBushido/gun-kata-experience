﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public GameObject hitParticleEffect;

    private float bulletSpeed = 10f;
    private bool canDie = false;

    private Vector3 lastFramePosition;
    private Rigidbody rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        Invoke("DeleteSelf", 5);
        Invoke("CanDie", 0.1f);
    }

    void Update () {

        lastFramePosition = transform.position;

        rigidbody.MovePosition(transform.position + (transform.forward * Time.deltaTime * bulletSpeed));
        //transform.position += transform.forward * Time.deltaTime * bulletSpeed;
                		
	}

    void DeleteSelf()
    {
        Destroy(gameObject);
    }

    void CanDie()
    {
        canDie = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (canDie)
        {
            Vector3 rayOrigin = lastFramePosition;
            RaycastHit hit;
            if (Physics.Raycast(rayOrigin, transform.forward, out hit))
            {
                if (collision.collider.GetComponent<ShatterToolkit.ShatterTool>() == null)
                {
                    //Instantiate(hitParticleEffect, collision.contacts[0].point, Quaternion.LookRotation(collision.contacts[0].normal), collision.transform);
                    Instantiate(hitParticleEffect, hit.point, Quaternion.LookRotation(hit.normal), collision.transform);
                }
            }


            Destroy(gameObject);
        }
    }
}
