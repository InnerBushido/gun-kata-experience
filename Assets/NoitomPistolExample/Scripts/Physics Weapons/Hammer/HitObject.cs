﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitObject : MonoBehaviour {

    //public CreateSparks sparkScript;
    public bool wasHit = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<MetalSurface>() != null)
        {
            Debug.Log("HAMMER HEAD HIT TRIGGER");
            //sparkScript.CreateSparksAtCollision();
            wasHit = true;
        }
    }

}
