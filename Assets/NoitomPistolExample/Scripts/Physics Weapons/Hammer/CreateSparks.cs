﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class CreateSparks : MonoBehaviour
{
    public GameObject sparksToEmit;
    public PhysicsGrabbable grabbable;
    public HitObject hitObject;

    private bool canMakeSparks = true;
    private float neededVelocityForSparks = 2;

    private Collision currentCollision;

    private void Start()
    {
        //grabbable = GetComponent<PhysicsGrabbable>();
    }

    private void Update()
    {
        if(hitObject.wasHit)
        {
            hitObject.wasHit = false;
            CreateSparksAtCollision();
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (grabbable && grabbable.m_CurrentHandGrabbing != null)
        //if(true)
        {
            Debug.Log("RELATIVE VELOCITY: " + collision.relativeVelocity.magnitude);
            if (collision.collider.GetComponent<MetalSurface>() &&
                canMakeSparks && collision.relativeVelocity.magnitude > neededVelocityForSparks)
            {
                currentCollision = collision;
            }
            else
            {
                currentCollision = null;
            }
        }
    }

    public void CreateSparksAtCollision()
    {
        if (currentCollision != null &&
            currentCollision.collider.GetComponent<MetalSurface>() && 
            canMakeSparks && currentCollision.relativeVelocity.magnitude > neededVelocityForSparks)
        {
            canMakeSparks = false;
            Invoke("MakeSparksAgain", 0.5f);
            Instantiate(sparksToEmit, currentCollision.contacts[0].point, Quaternion.LookRotation(currentCollision.contacts[0].normal), currentCollision.transform);
            currentCollision = null;
        }
    }

    private void MakeSparksAgain()
    {
        canMakeSparks = true;
    }

}
