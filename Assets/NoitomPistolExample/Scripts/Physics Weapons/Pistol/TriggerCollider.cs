﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class TriggerCollider : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GameObject bulletExitHole;
    public float triggerValue = 0;
    public bool hasShot = false;
    public PhysicsPistol pistol;
    public Collider[] colliders;

    private bool activated = false;

    private HingeJoint hingeJoint;
    //private ConfigurableJoint hingeJoint;

    private Quaternion maxRotation;
    private Quaternion minRotation;

    private float maxRotationValue;
    
    void Start()
    {
        hingeJoint = GetComponent<HingeJoint>();
        //hingeJoint = GetComponent<ConfigurableJoint>();

        maxRotation = transform.localRotation;
        maxRotationValue = hingeJoint.limits.min;

        Deactivated();
    }

    void Update()
    {
        if (activated)
        {
            triggerValue = Mathf.InverseLerp(0, maxRotationValue, hingeJoint.angle);

            if (triggerValue > 0.9f)
            {
                if (!hasShot)
                {
                    hasShot = true;
                    Instantiate(bulletPrefab, bulletExitHole.transform.position, bulletExitHole.transform.rotation);
                    Debug.Log("BULLET SHOT");

                    if (pistol.m_CurrentHandGrabbing != null)
                    {
                        PhysicsInteractionManager.Instance.VibrateHi5Hand(pistol.m_CurrentHandGrabbing.m_PhysicsHand.m_Hi5Hand);
                    }
                }
            }
            else if (triggerValue < 0.4f)
            {
                if (hasShot)
                {
                    hasShot = false;
                }
            }
        }
    }

    public void Activated()
    {
        foreach(Collider c in colliders)
        {
            c.enabled = true;
        }

        activated = true;
    }

    public void Deactivated()
    {
        foreach (Collider c in colliders)
        {
            c.enabled = false;
        }

        activated = false;
    }

}
