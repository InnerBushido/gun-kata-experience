﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class AmmoCollider : MonoBehaviour
{
    public AmmoColliderType m_AmmoColliderType;
    public GameObject m_ObjectAttachedTo;
    //public GameObject m_ColliderObject;

    public enum AmmoColliderType
    {
        AmmoCollider,
        BottomOfPistol,
        TopOfPistol,
        BottomAmmoRelease,
        PhysicalCollider
    }

}
