﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTrigger : MonoBehaviour {

    public HingeJoint hingeJoint;

    public float triggerValue = 0;

    private Quaternion startingRotation;
    private float maxRotation;// = 65;

    void Start()
    {
        startingRotation = transform.localRotation;
        maxRotation = hingeJoint.limits.min;
    }

    void Update()
    {
        triggerValue = Mathf.InverseLerp(0, hingeJoint.limits.min, hingeJoint.angle);

        transform.localRotation = startingRotation * Quaternion.Euler(Vector3.right * triggerValue * maxRotation);
    }

}
