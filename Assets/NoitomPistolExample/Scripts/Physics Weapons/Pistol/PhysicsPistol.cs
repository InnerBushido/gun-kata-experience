﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class PhysicsPistol : PhysicsGrabbable
{
    public TriggerCollider m_TriggerCollider;

    protected override void Grabbed()
    {
        base.Grabbed();

        m_TriggerCollider.Activated();

        // Added to fix issue where pulling the trigger with fingers forces hand forward, a consequence 
        // of doing everything with physics and having the hand floating and not being kinematic
        m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ReassignAllHingeJointRootConnectedBody(false);

        if (m_CurrentHandGrabbing.m_PhysicsHand.m_CompoundFingersParent != null)
        {
            m_CurrentHandGrabbing.m_PhysicsHand.m_CompoundFingersParent.gameObject.SetActive(true);
        }

    }

    protected override void Released()
    {
        base.Released();

        m_TriggerCollider.Deactivated();

        // Added to fix issue where pulling the trigger with fingers forces hand forward, a consequence 
        // of doing everything with physics and having the hand floating and not being kinematic
        m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ReassignAllHingeJointRootConnectedBody(true);

        if (m_CurrentHandGrabbing.m_PhysicsHand.m_CompoundFingersParent != null)
        {
            m_CurrentHandGrabbing.m_PhysicsHand.m_CompoundFingersParent.gameObject.SetActive(false);
        }

        // Added to fix issue when compound colliders are colliding with collidable and pistol is dropped,
        // the compound colliders wont call their colliders CollisionExit() which will make hands keep colliding.
        //m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ClearAllCollisions();
    }

}