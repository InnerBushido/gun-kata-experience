﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class PhysicsCombatPistolAmmo : PhysicsGrabbable
{
    public Transform m_PistolStartPosition;
    public new ConfigurableJoint m_ConfigJoint;
    //public bool m_CanLoadIntoPistol = true;
    public bool m_LoadedIntoPistol = false;
    public bool m_EjectedAmmo = false;
    public bool m_CanLoadIntoPistol = true;
    //public bool m_CheckForPhysicalCollision = false;

    public List<AmmoPhysicalCollider> m_PhysicalColliders;

    public PhysicsCombatPistol m_PistolAmmoIsIn;

    private Vector3 m_StartinglocalPosition;
    private Rigidbody m_CurrentPistolRigidbody;

    public bool previousValue = false;

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            TriggerAmmoEjectFromPistol();
        }
    }
    
    public void LateUpdate()
    {
        if (m_ConfigJoint != null && !m_EjectedAmmo)
        {
            if ((m_CollisionOccurred && !previousValue))//(m_EjectedAmmo && !previousValue) || 
            {
                previousValue = true;

                m_ConfigJoint.zMotion = ConfigurableJointMotion.Limited;
                m_ConfigJoint.autoConfigureConnectedAnchor = false;
                m_RigidBody.useGravity = false;
            }
            else if (!m_CollisionOccurred && previousValue)
            {
                previousValue = false;

                m_ConfigJoint.zMotion = ConfigurableJointMotion.Locked;
                m_ConfigJoint.autoConfigureConnectedAnchor = true;

                if (m_RigidBody != null) // Shouldn't happen but could happen
                {
                    m_RigidBody.useGravity = true;
                }
            }
        }
    }

    public void TogglePhysicalAmmoColliders(bool m_SetToActive)
    {
        if(!m_SetToActive)
        {
            //m_CheckForPhysicalCollision = true;

            foreach(AmmoPhysicalCollider c in m_PhysicalColliders)
            {
                c.m_IsColliding = true;
                c.GetComponent<Collider>().isTrigger = true;
            }
        }
    }

    private void TriggerAmmoEjectFromPistol()
    {
        if (m_LoadedIntoPistol)
        {
            m_RigidBody = gameObject.AddComponent<Rigidbody>();
            m_SavedRigidBody.SetRigidbody(m_RigidBody);
            m_RigidBody.useGravity = true;

            m_EjectedAmmo = true;
            //previousValue = false;

            if (m_CurrentPistolRigidbody != null)
            {
                AddConfigJoint(m_CurrentPistolRigidbody);
                m_ConfigJoint.zMotion = ConfigurableJointMotion.Limited;
                m_ConfigJoint.autoConfigureConnectedAnchor = false;
                m_CurrentPistolRigidbody = null;
            }

            m_LoadedIntoPistol = false;
        }
    }

    public void ToggleColliders(bool setTo)
    {
        foreach (AmmoPhysicalCollider c in m_PhysicalColliders)
        {
            c.GetComponent<Collider>().enabled = setTo;
        }
    }

    public void RemoveConfigJoint()
    {
        Destroy(m_ConfigJoint);

        m_SavedRigidBody = new RigidbodyState(m_RigidBody);
        m_RigidBody.useGravity = true;

        m_ConfigJoint = null;
    }

    public void AddConfigJoint(Rigidbody pistol)
    {
        m_ConfigJoint = gameObject.AddComponent<ConfigurableJoint>();

        m_CurrentPistolRigidbody = pistol;
        m_ConfigJoint.connectedBody = pistol;
        m_ConfigJoint.xMotion = ConfigurableJointMotion.Locked;
        m_ConfigJoint.yMotion = ConfigurableJointMotion.Locked;
        m_ConfigJoint.zMotion = ConfigurableJointMotion.Locked;
        m_ConfigJoint.angularXMotion = ConfigurableJointMotion.Locked;
        m_ConfigJoint.angularYMotion = ConfigurableJointMotion.Locked;
        m_ConfigJoint.angularZMotion = ConfigurableJointMotion.Locked;
        m_ConfigJoint.enableCollision = true;

        SoftJointLimit tempLimit = new SoftJointLimit { limit = 0.1f };
        m_ConfigJoint.linearLimit = tempLimit;
    }

    public override void Start()
    {
        m_StartinglocalPosition = m_PistolStartPosition.localPosition;

        base.Start();
    }

    public override void ReleaseGrabbable(PhysicsHandGrabber bindToHand)
    {
        base.ReleaseGrabbable(bindToHand);
    }

    protected override IEnumerator ToggleCollidersOn()
    {
        //return base.ToggleCollidersOn();

        if (!m_CanLoadIntoPistol)
        {
            TogglePhysicalColliders(false);
        }

        yield return new WaitForSeconds(m_TimeUntilCollidable);

        if (m_CanLoadIntoPistol)
        {
            TogglePhysicalColliders(false);
            m_CanBeGrabbed = true;
        }

        m_IsGrabbed = false;
        m_PickUpTriggerCollider.enabled = true;
        m_CurrentHandGrabbing = null;
    }

}
