﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class AmmoPhysicalCollider : AmmoCollider
{
    public PhysicsCombatPistolAmmo m_PistolAmmo;

    public bool m_IsColliding = false;
    public bool m_CollidingThisFrame = false;

    public void LateUpdate()
    {
        if(!m_CollidingThisFrame && m_IsColliding)
        {
            Debug.Log(gameObject.name + " is no longer colliding.");
            m_IsColliding = false;
            GetComponent<Collider>().isTrigger = false;
        }
        
        m_CollidingThisFrame = false;
    }

    //void OnCollisionStay(Collision collisionInfo)
    void OnTriggerStay(Collider other)
    {

        if (!other.isTrigger)
        {
            if (m_IsColliding)// && m_PistolAmmo.m_CheckForPhysicalCollision)
            {
                m_IsColliding = true;
                m_CollidingThisFrame = true;
            }
        }
    }

}
