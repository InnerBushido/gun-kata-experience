﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class PhysicsCombatPistol : PhysicsPistol
{
    public Transform m_AmmoParent;
    public Transform m_AmmoStartPosition;
    public Transform m_AmmoEndPosition;

    protected override void Grabbed()
    {
        //base.Grabbed();

        m_TriggerCollider.Activated();

        // Added to fix issue where pulling the trigger with fingers forces hand forward, a consequence 
        // of doing everything with physics and having the hand floating and not being kinematic
        m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ReassignAllHingeJointRootConnectedBody(false);

    }

    protected override void Released()
    {
        //base.Released();

        m_TriggerCollider.Deactivated();

        // Added to fix issue where pulling the trigger with fingers forces hand forward, a consequence 
        // of doing everything with physics and having the hand floating and not being kinematic
        m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ReassignAllHingeJointRootConnectedBody(true);

        // Added to fix issue when compound colliders are colliding with collidable and pistol is dropped,
        // the compound colliders wont call their colliders CollisionExit() which will make hands keep colliding.
        m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ClearAllCollisions();
    }

    protected override IEnumerator ToggleCollidersOn()
    {
        //return base.ToggleCollidersOn();
        yield return new WaitForSeconds(m_TimeUntilCollidable);

        //TogglePhysicalColliders(false);
        foreach (Collider c in m_PickUpPhysicalColliders)
        {
            m_CurrentHandGrabbing.m_PhysicsHand.m_physicsRigidBody.ReassignJointLayersName(false);
            //c.gameObject.layer = LayerMask.NameToLayer("Default");
        }


        m_CanBeGrabbed = true;
        m_IsGrabbed = false;
        m_PickUpTriggerCollider.enabled = true;
        m_CurrentHandGrabbing = null;
    }

}