﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolAmmoCollider : AmmoCollider
{
    public PhysicsCombatPistolAmmo m_PistolAmmo;
    public bool m_IsFullyLoadedIntoPistol = false;
    public bool m_IsReleasedFromPistol = true;

    public AmmoCollider ammoCollider;

    internal Transform m_OriginalParent;
    
    void Start()
    {
        m_OriginalParent = m_PistolAmmo.transform.parent;

        //TriggerAmmoAttachToPistol(ammoCollider);
    }

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("COLLISION OCCURRED");

        if (m_AmmoColliderType == AmmoColliderType.AmmoCollider)
        {
            AmmoCollider otherAmmoCollider = other.GetComponent<AmmoCollider>();

            if (otherAmmoCollider != null)
            {
                if (otherAmmoCollider.m_AmmoColliderType == AmmoColliderType.BottomOfPistol &&
                    m_PistolAmmo.m_CanLoadIntoPistol)
                {
                    TriggerAmmoAttachToPistol(otherAmmoCollider);
                }
                else if (otherAmmoCollider.m_AmmoColliderType == AmmoColliderType.BottomAmmoRelease &&
                        !m_PistolAmmo.m_CanLoadIntoPistol && !m_IsFullyLoadedIntoPistol && !m_IsReleasedFromPistol)
                {
                    TriggerAmmoEjectFromPistol(otherAmmoCollider);
                }
                else if (otherAmmoCollider.m_AmmoColliderType == AmmoColliderType.TopOfPistol &&
                        !m_PistolAmmo.m_CanLoadIntoPistol && !m_IsFullyLoadedIntoPistol)
                {
                    TriggerAmmoDetachFromPistol(otherAmmoCollider);
                }
                else if (otherAmmoCollider.m_AmmoColliderType == AmmoColliderType.BottomOfPistol &&
                    !m_PistolAmmo.m_CanLoadIntoPistol && m_IsFullyLoadedIntoPistol)
                {
                    TriggerAmmoEjectFromPistol(otherAmmoCollider);
                }
            }
        }
    }

    private void TriggerAmmoEjectFromPistol(AmmoCollider otherAmmoCollider)
    {
        Debug.Log("FULLY EJECTED");
        //m_PistolAmmo.ToggleColliders(true);
        m_PistolAmmo.RemoveConfigJoint();
        m_PistolAmmo.m_EjectedAmmo = false;
        m_PistolAmmo.transform.parent = m_OriginalParent;

        m_PistolAmmo.m_CanBeGrabbed = true;
        TogglePhysicalColliders(m_PistolAmmo.m_PistolAmmoIsIn, false);
        m_PistolAmmo.m_PistolAmmoIsIn = null;
        m_IsFullyLoadedIntoPistol = false;
        m_IsReleasedFromPistol = true;

        StartCoroutine(AmmoEjected());
    }

    IEnumerator AmmoEjected()
    {
        yield return new WaitForSeconds(0.1f);
        m_PistolAmmo.m_CanLoadIntoPistol = true;
        m_PistolAmmo.m_CanBeGrabbed = true;
        //TogglePhysicalColliders(m_PistolAmmo.m_PistolAmmoIsIn, false);
        //m_PistolAmmo.m_PistolAmmoIsIn = null;

        m_PistolAmmo.ToggleColliders(true);
    }

    private void TriggerAmmoDetachFromPistol(AmmoCollider otherAmmoCollider)
    {
        //PhysicsCombatPistol pistol = otherAmmoCollider.GetComponent<Collider>().attachedRigidbody.GetComponent<PhysicsCombatPistol>();
        PhysicsCombatPistol pistol = m_PistolAmmo.m_PistolAmmoIsIn;

        m_PistolAmmo.ToggleColliders(false);

        m_PistolAmmo.RemoveConfigJoint();
        Destroy(m_PistolAmmo.m_RigidBody);

        m_PistolAmmo.transform.position = pistol.m_AmmoEndPosition.position;
        m_PistolAmmo.transform.rotation = pistol.m_AmmoEndPosition.rotation;

        m_IsFullyLoadedIntoPistol = true;
        m_PistolAmmo.m_LoadedIntoPistol = true;
    }

    private void TriggerAmmoAttachToPistol(AmmoCollider otherAmmoCollider)
    {
        m_PistolAmmo.m_CanLoadIntoPistol = false;
        m_PistolAmmo.m_CanBeGrabbed = false;
        m_IsReleasedFromPistol = false;

        // If ammo is held
        if (m_PistolAmmo.m_CurrentHandGrabbing != null)
        {
            m_PistolAmmo.m_CurrentHandGrabbing.Release();
        }

        AttachAmmoToPistol(otherAmmoCollider);
        
    }

    private void AttachAmmoToPistol(AmmoCollider otherAmmoCollider)
    {
        //PhysicsCombatPistol pistol = otherAmmoCollider.GetComponent<Collider>().attachedRigidbody.GetComponent<PhysicsCombatPistol>();
        PhysicsCombatPistol pistol = null;

        if (otherAmmoCollider.m_ObjectAttachedTo != null)
        {
            pistol = otherAmmoCollider.m_ObjectAttachedTo.GetComponent<PhysicsCombatPistol>();
        }

        if (pistol != null)// && pistol.m_CurrentHandGrabbing != null)
        {
            m_PistolAmmo.m_PistolAmmoIsIn = pistol;

            TogglePhysicalColliders(pistol);

            //m_PistolAmmo.transform.localScale = new Vector3(1, 1, 1);

            m_PistolAmmo.transform.parent = pistol.m_AmmoParent;
            
            m_PistolAmmo.transform.position = pistol.m_AmmoStartPosition.position;
            m_PistolAmmo.transform.rotation = pistol.m_AmmoStartPosition.rotation;
            
            m_PistolAmmo.TogglePhysicalAmmoColliders(false);
            m_PistolAmmo.AddConfigJoint(otherAmmoCollider.GetComponent<Collider>().attachedRigidbody);
            //m_PistolAmmo.m_RigidBody.isKinematic = true;

            //m_PistolAmmo.transform.parent = pistol.m_AmmoParent;

        }

        //Debug.Break();
    }

    private void TogglePhysicalColliders(PhysicsCombatPistol pistol, bool toggleNewLayer = true)
    {
        if (toggleNewLayer)
        {
            foreach (Collider c in m_PistolAmmo.m_PickUpPhysicalColliders)
            {
                c.gameObject.layer = LayerMask.NameToLayer("Default2");
                //c.enabled = false;
            }

            foreach (Collider c in pistol.m_PickUpPhysicalColliders)
            {
                c.gameObject.layer = LayerMask.NameToLayer("Default3");
            }
        }
        else
        {
            foreach (Collider c in m_PistolAmmo.m_PickUpPhysicalColliders)
            {
                c.gameObject.layer = LayerMask.NameToLayer("Default");
            }

            foreach (Collider c in pistol.m_PickUpPhysicalColliders)
            {
                if (pistol.m_IsGrabbed)
                {
                    c.gameObject.layer = LayerMask.NameToLayer("GrabbableInHand");
                }
                else
                {
                    c.gameObject.layer = LayerMask.NameToLayer("Default");
                }
            }
        }
    }

}
