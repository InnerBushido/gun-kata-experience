﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class PhysicsCuttingSword : PhysicsGrabbable
{
    public SwordCutting m_SwordCutting;
    public Collider[] m_BladeCollider;

    public Transform m_GrabPointRight;
    public Transform m_GrabPointLeft;
    
    public bool m_ShowDebugVisualization = true;
    public GameObject m_DebugInfo;

    private Vector3 m_RightHandOffsetLocalPosition;

    private void Update()
    //public override void Update()
    {
        //base.Update();

        //HandleDebug();
        HandleLayerSwitch();
        //HandleTransformUpdate();
    }

    //private void HandleTransformUpdate()
    //{
    //    FigureOutNestedRotation();
    //}

    //private void FigureOutNestedRotation()
    //{
    //    if (m_RightHand != null)
    //    {
    //        transform.rotation = m_RightHand.rotation * m_GrabPointRight.localRotation;// * Quaternion.Euler(m_AddRotationOffsetWhenHeld);
    //        m_RightHandOffsetLocalPosition = m_GrabPointRight.position - transform.position;
    //        transform.position = m_RightHand.position - m_RightHandOffsetLocalPosition;
    //    }
    //}

    //private void HandleDebug()
    //{
    //    if (m_CurrentHeldType != HeldType.NotGrabbed)
    //    {
    //        m_DebugInfo.SetActive(m_ShowDebugVisualization);
    //    }
    //    else
    //    {
    //        m_DebugInfo.SetActive(false);
    //    }        
    //}

    private void HandleLayerSwitch()
    {
        if (m_SwordCutting.swordSwinging)
        {
            foreach (Collider c in m_BladeCollider)
            {
                c.gameObject.layer = LayerMask.NameToLayer("SwordCutting");
            }
        }
        else
        {
            foreach (Collider c in m_BladeCollider)
            {
                if (m_IsGrabbed)
                {
                    c.gameObject.layer = LayerMask.NameToLayer("GrabbableInHand");
                }
                else
                {
                    c.gameObject.layer = LayerMask.NameToLayer("Default");
                }
            }
        }
    }


}
