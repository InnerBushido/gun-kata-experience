﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5.PhysicsInteraction;

public class ResetInteractables : MonoBehaviour {

    public PhysicsCollidable[] m_AllInteractables;
    public List<Resetable> m_Resetable = new List<Resetable>();

    public List<PhysicsCollidable> m_AllJengaCubes = new List<PhysicsCollidable>();
    public List<Resetable> m_ResetableJengaCubes = new List<Resetable>();

    public GameObject m_JengaParent;
    
	void Start () {

        m_AllInteractables = FindObjectsOfType<PhysicsCollidable>();

        AssignJengaCubes();

        foreach (PhysicsCollidable c in m_AllInteractables)
        {
            if (c.GetComponent<Resetable>() == null && c.GetComponent<IgnoreResetable>() == null)
            {
                m_Resetable.Add(c.gameObject.AddComponent<Resetable>());
                c.gameObject.GetComponent<Resetable>().Initiate(Resetable.ResetType.defaultCollidable);
            }
        }
        
        StartCoroutine(CheckPositions());
	}

    void AssignJengaCubes()
    {
        if(m_JengaParent != null)
        {
            m_JengaParent.GetComponentsInChildren(false, m_AllJengaCubes);

            foreach(PhysicsCollidable c in m_AllJengaCubes)
            {
                m_ResetableJengaCubes.Add(c.gameObject.AddComponent<Resetable>());
                c.gameObject.GetComponent<Resetable>().Initiate(Resetable.ResetType.JengaCube);
            }
        }
    }
    	
    IEnumerator CheckPositions()
    {
        yield return new WaitForSeconds(2);
        foreach (Resetable c in m_Resetable)
        {
            if (c.transform.position.y < 0.2f)
            {
                c.Reset();
            }
        }
        StartCoroutine(CheckPositions());
    }

    public void ResetAllJengaCubes()
    {
        foreach(Resetable c in m_ResetableJengaCubes)
        {
            c.Reset();
        }
    }
}
