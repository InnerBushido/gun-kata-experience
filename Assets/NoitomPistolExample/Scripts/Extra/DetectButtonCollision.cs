﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectButtonCollision : MonoBehaviour
{
    public ResetInteractables m_ResetManager;
    public CoffeeSpawner m_CoffeSpawner;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ResetJengaSphereCollider")
        {
            Debug.Log("RESETTING JENGA BLOCKS");
            m_ResetManager.ResetAllJengaCubes();

            if(m_CoffeSpawner != null)
            {
                m_CoffeSpawner.PourCoffee();
            }
        }
    }

}
