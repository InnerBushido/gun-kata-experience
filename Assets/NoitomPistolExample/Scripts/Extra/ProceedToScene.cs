﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using HI5.VRCalibration;

public class ProceedToScene : MonoBehaviour {

    public CalibrationStateMachine m_CalibrationStateMachine;

    private void OnEnable()
    {
        m_CalibrationStateMachine.OnStateEnter += HandleCalibrationComplete;
    }

    private void OnDisable()
    {
        m_CalibrationStateMachine.OnStateEnter -= HandleCalibrationComplete;
    }
    

    private void HandleCalibrationComplete(CalibrationState stateEnter)
    {
        if (stateEnter == CalibrationState.Exit)
            GoToNextScene();
    }

    public void GoToNextScene()
    {
        StartCoroutine(StartScene());
    }

    IEnumerator StartScene()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(Application.loadedLevel + 1);
    }

}
