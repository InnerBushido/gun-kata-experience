﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resetable : MonoBehaviour {

    public Vector3 m_StartingPosition;
    public Quaternion m_StartingRotation;

    public ResetType m_CurrentType;

    public enum ResetType
    {
        defaultCollidable,
        JengaCube
    }

    private void Start()
    {
        m_StartingPosition = transform.position;
        m_StartingRotation = transform.rotation;
    }

    public void Initiate(ResetType _type)
    {
        m_CurrentType = _type;
    }

    public void Reset()
    {
        transform.position = m_StartingPosition;
        transform.rotation = m_StartingRotation;

        Vector3 absLocalScale;
        absLocalScale = new Vector3(Mathf.Abs(transform.localScale.x), Mathf.Abs(transform.localScale.y), Mathf.Abs(transform.localScale.z));
        transform.localScale = absLocalScale;

        if (GetComponent<Rigidbody>() != null)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }


}
