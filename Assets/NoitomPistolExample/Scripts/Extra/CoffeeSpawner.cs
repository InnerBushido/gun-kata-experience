﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeSpawner : MonoBehaviour {

    public GameObject m_CoffeePrefab;

    public int m_AmountToPour = 20;

    private int m_AmountPoured = 0;
    private List<GameObject> m_Coffee;

    private void Start()
    {
        PourCoffee();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            PourCoffee();
        }
    }

    public void PourCoffee()
    {
        if(m_Coffee != null)
        {
            DeleteAllCoffee();
        }

        m_Coffee = new List<GameObject>();

        StartCoroutine(PourCoffeeCoroutine());
    }

    IEnumerator PourCoffeeCoroutine()
    {
        GameObject temp = Instantiate(m_CoffeePrefab, transform.position, transform.rotation);
        m_Coffee.Add(temp);
        //temp.transform.parent = transform;
        temp.GetComponent<Rigidbody>().velocity = temp.transform.forward * 1;
        m_AmountPoured++;
        yield return new WaitForSeconds(0.025f);
        if (m_AmountPoured < m_AmountToPour)
        {
            StartCoroutine(PourCoffeeCoroutine());
        }
        else
        {
            m_AmountPoured = 0;
        }
    }

    private void DeleteAllCoffee()
    {
        for(int i = 0; i < m_Coffee.Count; i++)
        {
            Destroy(m_Coffee[i]);
        }
    }

}
