﻿using UnityEngine;
using UnityEngine.SceneManagement;
using HI5.PhysicsInteraction;

namespace Utilities
{
    public class InputManager : SingletonBehaviour<InputManager>
    {
        public ResetInteractables resetManager;

        // UI Button //
        public void ButtonDown(int _num)
        {
            StateMachine.SendButtonDown(_num);
        }
        // UI Button //
        public void ButtonUp(int _num)
        {
            StateMachine.SendButtonUp(_num);
        }

        void Awake()
        {
            DontDestroy();
            Application.runInBackground = true;
        }

        // debug input
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(Application.loadedLevel);
            }

            if (Input.GetKeyDown(KeyCode.J))
            {
                resetManager.ResetAllJengaCubes();
            }

            if (Input.GetKeyDown(KeyCode.M))
            {
                Cursor.visible = !Cursor.visible;
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                SceneManager.LoadScene(1);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                SceneManager.LoadScene(2);
            }

            if(Input.GetKeyDown(KeyCode.H))
            {
                // Flip Whether Hands Appear or not
                PhysicsInteractionManager.Instance.ShowCollisionHands = !PhysicsInteractionManager.Instance.ShowCollisionHands;
            }

        }
    }
}
