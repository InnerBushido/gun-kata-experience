﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Utilities
{
    public class GameManager : SingletonBehaviour<GameManager>
    {
        public GameObject[] states;

        IEnumerator Start()
        {
            yield return null;
            GoToStartState();
        }

        public static IState GetState(int _num)
        {
            return Instance.states[_num].GetComponent<IState>();
        }

        public static void NextState()
        {
            StateMachine.NextState();
        }

        public static void GoToStartState()
        {
            StateMachine.ChangeState(Instance.states[0].GetComponent<IState>());
        }

    }
}
