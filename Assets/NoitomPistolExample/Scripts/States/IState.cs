﻿using System.Collections;
using UnityEngine;

namespace Utilities
{
    public interface IState
    {
        IEnumerator iEnter();
        IEnumerator iExit();
        IState GetNextState();
        void Execute();

        void ButtonDown(int _num);
        void ButtonUp(int _num);
    }
}