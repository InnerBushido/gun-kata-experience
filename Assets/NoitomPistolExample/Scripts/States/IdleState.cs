﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5;

public class IdleState : BaseState {

    public override void Execute()
    {
        base.Execute();
    }

    public override IEnumerator iEnter()
    {
        Debug.Log("IdleState Enter: " + Time.time);

        // Code Here
        //HI5_Calibration.LoadCalibrationData(HI5_Calibration.DefaultPath);

        return base.iEnter();
    }

    public override IEnumerator iExit()
    {
        Debug.Log("IdleState Exit: " + Time.time);

        // Code Here

        yield return StartCoroutine(base.iExit());
    }

    protected override void StartReset()
    {
        base.StartReset();
    }

    protected override void EndReset()
    {
        base.EndReset();
    }

}
