﻿using UnityEngine;
using System.Collections;
using Utilities;

public class BaseState : MonoBehaviour, IState
{
    public GameObject nextState;
    public BaseState previousPanel;

    internal bool canProceed = false;

    public virtual IEnumerator iEnter()
    {
        StartReset();
        yield return null;
    }

    public virtual void Execute() { }

    public virtual IEnumerator iExit()
    {
        //yield return StartCoroutine(LeavePanel());

        EndReset();
        yield return null;
    }

    protected virtual void StartReset() {}
    protected virtual void EndReset() { }

    public void ButtonDown(int _num)
    {
        Debug.Log(_num + " ButtonDown: " + Time.time);
    }

    public void ButtonUp(int _num)
    {
        Debug.Log(_num + " ButtonDown: " + Time.time);
    }

    public IState GetNextState()
    {
        return nextState.GetComponent<IState>();
    }

    //public IEnumerator LeavePanel()
    //{
    //    yield return null;
    //}

    public void ResetPanel()
    {
        canProceed = false;
    }
    
    // Use this when User Triggers Next State
    public virtual void UserTriggeredNextState()
    {
        if (canProceed)
        {
            canProceed = false;

            GameManager.NextState();
        }
    }

    // Use this when State is done transitioning
    public virtual void SetCanProceed()
    {
        canProceed = true;
    }

    public virtual void PreviousButtonClicked()
    {
        if (previousPanel != null)
        {
            StateMachine.ChangeState(previousPanel);
        }
    }

}
