﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwohandedTest : MonoBehaviour
{
    public Transform m_GrabPointRight;
    public Transform m_GrabPointLeft;

    public Transform m_RightHand;
    public Transform m_LeftHand;

    public Transform m_LeftHandReposition;

    public RotationType m_RotationType;
    public enum RotationType
    {
        OneAxisRotation,
        TwoAxisConeRotation
    }
    
    private float m_PitchMin = -30;
    private float m_PitchMax = 30;

    private float m_RollMin = -15;
    private float m_RollMax = 15;

    public float m_CurrentPitchNormalized;

    private Vector3 m_RightHandOffsetLocalPosition;
    private Quaternion m_RightHandStartingRotation;
    private Quaternion m_CurrentAngledRotation = new Quaternion();
    private Quaternion m_CurrentHandAngledRotation = new Quaternion();

    private Ray ray;
    private float rayDistance;
    
    // For Debugging
    //public int currentRotationVal = 0;
    public GameObject handPlane;
    public GameObject swordPlane;
    public GameObject swordReposition;
    public exampleCircle exampleCircle;
    public Transform LeftHandRightPlaneReposition;


    private void Start()
    {
        m_RightHandStartingRotation = m_RightHand.rotation;

        //FigureOutNestedRotation();
        //transform.position = m_RightHand.position;
        //transform.rotation = m_RightHand.rotation * m_GrabPointRight.localRotation;

    }

    private void Update()
    {
        ComplexNormalizeRotationValues();
        
        RemoveLocalSwordDisparity();
        //m_LeftHandReposition.position = m_LeftHand.position;

        //SimpleRotationBetweenTwoPoints();
        //FigureOutNestedRotation();
        //ComplexRotationBetweenTwoPoints();

        if (m_CurrentPitchNormalized > 0 && m_CurrentPitchNormalized < 1)
        {
            ComplexRotationBetweenTwoPoints();
        }
        else
        {
            if (m_RotationType == RotationType.OneAxisRotation)
            {
                //HandleOneAxisInput();

                //FigureOutNestedRotation();
                OneAxisSwordRotation();
            }
            else if (m_RotationType == RotationType.TwoAxisConeRotation)
            {
                ComplexRotationBetweenTwoPoints();
            }
        }
    }

    //private void HandleOneAxisInput()
    //{
    //    if(Input.GetKeyDown(KeyCode.N))
    //    {
    //        currentRotationVal++;

    //        if(currentRotationVal > 3)
    //        {
    //            currentRotationVal = 0;
    //        }
    //    }
    //}

    private void SimpleRotationBetweenTwoPoints()
    {
        transform.position = m_RightHand.position;
        transform.rotation = Quaternion.LookRotation(m_RightHand.position - m_LeftHand.position, m_RightHand.right);
    }

    private void FigureOutNestedRotation()
    {
        //transform.rotation = m_RightHand.rotation * m_GrabPointRight.localRotation;
        m_RightHandOffsetLocalPosition = m_GrabPointRight.position - transform.position;
        transform.position = m_RightHand.position - m_RightHandOffsetLocalPosition;
    }

    private void ComplexRotationBetweenTwoPoints()
    {
        //transform.position = m_RightHand.position;

        m_CurrentAngledRotation = Quaternion.LookRotation(m_RightHand.position - m_LeftHandReposition.position, m_RightHand.right);
        transform.rotation = m_CurrentAngledRotation;

        FigureOutNestedRotation();

        //Needed so when sword stops rotating, we know the angle of the hand
        m_CurrentHandAngledRotation = m_RightHand.rotation;
    }

    private void OneAxisSwordRotation()
    {
        transform.position = m_RightHand.position;

        Quaternion newRotation;

        newRotation = m_RightHand.rotation;
        newRotation *= Quaternion.Inverse(m_RightHandStartingRotation);
        newRotation *= Quaternion.Inverse(m_CurrentHandAngledRotation * Quaternion.Inverse(m_RightHandStartingRotation));
        newRotation *= m_CurrentAngledRotation;

        //switch (currentRotationVal)
        //{
        //    case 1:
        //        newRotation *= Quaternion.Inverse(m_RightHandStartingRotation);
        //        break;
        //    case 2:
        //        newRotation *= Quaternion.Inverse(m_RightHandStartingRotation);
        //        newRotation *= Quaternion.Inverse(m_CurrentHandAngledRotation * Quaternion.Inverse(m_RightHandStartingRotation));

        //        break;
        //    case 3:
        //        newRotation *= Quaternion.Inverse(m_RightHandStartingRotation);
        //        newRotation *= Quaternion.Inverse(m_CurrentHandAngledRotation * Quaternion.Inverse(m_RightHandStartingRotation));
        //        newRotation *= m_CurrentAngledRotation;
        //        break;
        //}

        transform.rotation = newRotation;
    }

    private void RemoveLocalSwordDisparity()
    {
        //Vector3 vectorOffset = m_RightHand.position - m_LeftHand.position;
        //m_LeftHandReposition.position = m_RightHand.position - vectorOffset;
        m_LeftHandReposition.position = m_LeftHand.position;
        
        if (m_RotationType == RotationType.OneAxisRotation)
        {
            RepositionRespectiveToRightHand();
        }
        else if (m_RotationType == RotationType.TwoAxisConeRotation)
        {
            RepositionRespectiveToRightHand();
            RepositionUnderRightHand();
            //if (m_CurrentPitchNormalized <= 0 || m_CurrentPitchNormalized >= 1)
            //{
            //    RepositionRespectiveToRightHand();
            //    RepositionUnderRightHand();
            //}
        }
    }

    private void RepositionRespectiveToRightHand()
    {
        Plane rightHandPlane = new Plane(-m_RightHand.up, m_RightHand.position);
        Vector3 rightHandVector = rightHandPlane.normal;
        Vector3 leftHandVector;

        rightHandVector.Normalize();
        rightHandVector *= 0.05f;

        leftHandVector = -rightHandVector;

        ray = new Ray(m_LeftHand.position, leftHandVector);
        ray.direction.Normalize();

        // Right hand is to "right" of left hand
        if (rightHandPlane.Raycast(ray, out rayDistance))
        {
            LeftHandRightPlaneReposition.position = ray.origin + (ray.direction * rayDistance);
            Debug.DrawLine(m_LeftHand.position, m_LeftHand.position + leftHandVector, Color.yellow);
        }
        else
        {
            //Right hand is to "left of left hand
            ray = new Ray(m_LeftHand.position, rightHandVector);
            ray.direction.Normalize();

            if (rightHandPlane.Raycast(ray, out rayDistance))
            {
                LeftHandRightPlaneReposition.position = ray.origin + (ray.direction * rayDistance);
                Debug.DrawLine(m_LeftHand.position, m_LeftHand.position + rightHandVector, Color.yellow);
            }
        }

        // Added for OneAxisRotation
        m_LeftHandReposition.position = LeftHandRightPlaneReposition.position;

        //Debug.Log("Distance is: " + m_RepositionLastPosition.magnitude);
        Debug.DrawLine(m_RightHand.position, m_RightHand.position + rightHandVector, Color.green);

        handPlane.transform.position = m_RightHand.position;
        handPlane.transform.LookAt(m_RightHand.position + rightHandVector);
        handPlane.transform.rotation *= Quaternion.Euler(90, 0, 0);
    }

    private void RepositionUnderRightHand()
    {
        Vector3 rightHandDownVector = -m_RightHand.forward;
        Plane swordPlanePerpendicular = new Plane(-m_RightHand.forward, LeftHandRightPlaneReposition.position);
        Vector3 swordRepositionToLeftHandVector;

        swordPlane.transform.position = LeftHandRightPlaneReposition.position;
        swordPlane.transform.rotation = Quaternion.LookRotation(-swordPlanePerpendicular.normal);
        swordPlane.transform.rotation *= Quaternion.Euler(90, 0, 0);

        rightHandDownVector.Normalize();
        rightHandDownVector *= 0.05f;

        ray = new Ray(m_RightHand.position, rightHandDownVector);
        ray.direction.Normalize();

        // Right hand is "above" Sword Plane
        if (swordPlanePerpendicular.Raycast(ray, out rayDistance))
        {
            swordReposition.transform.position = ray.origin + (ray.direction * rayDistance);
        }
        else
        {
            // Right hand is "below" Sword Plane
            ray = new Ray(m_RightHand.position, -rightHandDownVector);
            ray.direction.Normalize();

            if (swordPlanePerpendicular.Raycast(ray, out rayDistance))
            {
                swordReposition.transform.position = ray.origin + (-ray.direction * rayDistance);
            }
        }

        swordRepositionToLeftHandVector = m_LeftHand.position - swordReposition.transform.position;
        swordRepositionToLeftHandVector.Normalize();
        swordRepositionToLeftHandVector *= 0.05f;
        Debug.DrawLine(swordReposition.transform.position, swordReposition.transform.position + swordRepositionToLeftHandVector, Color.yellow);

        //swordReposition.transform.LookAt(swordReposition.transform.position + swordRepositionToLeftHandVector);
        //swordReposition.transform.rotation = Quaternion.LookRotation(swordPlanePerpendicular.normal);
        swordReposition.transform.rotation = m_RightHand.rotation;
        swordReposition.transform.rotation *= Quaternion.Euler(90, 0, 0);
        swordReposition.transform.rotation *= Quaternion.Euler(0, 90, 0);

        //FigureOutConePositionForSword(swordRepositionToLeftHandVector);
        FigureOutComplexConePositionForSword(swordRepositionToLeftHandVector);
    }

    private void FigureOutConePositionForSword(Vector3 swordRepositionToLeftHandVector)
    {
        float r;
        float h;
        float thetaR;

        thetaR = m_PitchMax;
        h = Vector3.Distance(swordReposition.transform.position, m_RightHand.transform.position);
        r = h * Mathf.Tan(thetaR * Mathf.Deg2Rad);

        //Debug.Log("Height of SwordReposition to rightHand is: " + h);
        //Debug.Log("Radius of SwordReposition is: " + r);

        exampleCircle.xradius = r;
        exampleCircle.yradius= r;

        swordRepositionToLeftHandVector.Normalize();
        m_LeftHandReposition.position = swordReposition.transform.position + (swordRepositionToLeftHandVector * r);
    }

    private void FigureOutComplexConePositionForSword(Vector3 swordRepositionToLeftHandVector)
    {
        float R, r, h, l;
        float thetaR, thetar;
        float angle;
        float distanceToHand;

        thetaR = m_PitchMax;
        thetar = m_RollMax;
        h = Vector3.Distance(swordReposition.transform.position, m_RightHand.transform.position);
        R = h * Mathf.Tan(thetaR * Mathf.Deg2Rad);
        r = h * Mathf.Tan(thetar * Mathf.Deg2Rad);

        angle = Vector3.Angle(swordReposition.transform.forward, swordRepositionToLeftHandVector);
        angle *= Mathf.Deg2Rad;

        // L * L = ( 1+tan²(θ) ）* R² * r² / ( r² + R² * tan²(θ) )
        l = Mathf.Sqrt(((1 + (Mathf.Pow(Mathf.Tan(angle), 2))) * Mathf.Pow(R, 2) * Mathf.Pow(r, 2)) / (Mathf.Pow(r, 2) + (Mathf.Pow(R, 2) * (Mathf.Pow(Mathf.Tan(angle), 2)))));

        //Debug.Log("Height of SwordReposition to rightHand is: " + h);
        //Debug.Log("Radius of SwordReposition is: " + r);
        //Debug.Log("Angle of SwordReposition is: " + angle);
        //Debug.Log("Length of SwordReposition is: " + l);

        exampleCircle.xradius = r;
        exampleCircle.yradius = R;

        distanceToHand = Vector3.Distance(m_LeftHand.position, swordReposition.transform.position);

        if (distanceToHand > l)
        {
            swordRepositionToLeftHandVector.Normalize();
            m_LeftHandReposition.position = swordReposition.transform.position + (swordRepositionToLeftHandVector * l);
        }
        else
        {
            m_LeftHandReposition.position = m_LeftHand.position;
        }
    }

    private void ComplexNormalizeRotationValues()
    {
        Vector3 lookAtHandVector = new Vector3();
        Vector3 rightHandDownVector = -m_RightHand.forward;
        float angleFromHandToReposition;

        if (m_RotationType == RotationType.OneAxisRotation)
        {
            lookAtHandVector = m_RightHand.position - m_LeftHandReposition.position;
        }
        else if (m_RotationType == RotationType.TwoAxisConeRotation)
        {
            lookAtHandVector = m_RightHand.position - m_LeftHand.position;
        }

        lookAtHandVector.Normalize();
        lookAtHandVector *= 0.05f;

        rightHandDownVector.Normalize();
        rightHandDownVector *= 0.05f;

        //Debug.DrawLine(m_LeftHandReposition.position, m_RightHand.position, Color.yellow);
        Debug.DrawLine(m_LeftHand.position, m_LeftHand.position + lookAtHandVector, Color.red);

        Debug.DrawLine(m_RightHand.position, m_RightHand.position + rightHandDownVector, Color.blue);

        angleFromHandToReposition = Vector3.Angle(rightHandDownVector, lookAtHandVector);
        angleFromHandToReposition = angleFromHandToReposition - 180;

        //Debug.Log("ANGLE: " + angleFromHandToReposition);

        m_CurrentPitchNormalized = Mathf.InverseLerp(m_PitchMin, m_PitchMax, angleFromHandToReposition);
    }

}
