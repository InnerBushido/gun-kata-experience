﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandOffsetter : MonoBehaviour
{
    public Transform m_HandOffset;
    public Transform m_HandToOffset;
    public Transform m_OffsetRotator;
    public Transform m_HandRotation;
    public Transform m_HandOffsetter;

    private Vector3 m_InitialOffset = new Vector3();
    private Transform m_OriginalHandParent;

    public void Start()
    {
        m_OriginalHandParent = m_HandToOffset.parent;

        m_InitialOffset = m_HandOffset.localPosition;

        m_OffsetRotator.localPosition = m_InitialOffset;
        m_OffsetRotator.localRotation = m_HandOffset.localRotation;
        m_HandOffsetter.localPosition = m_InitialOffset;
        m_HandOffsetter.localRotation = m_HandOffset.localRotation;

        m_HandOffset.position = Vector3.zero;
        m_HandOffset.rotation = Quaternion.identity;
    }

    public void LateUpdate()
    {
        if(m_HandToOffset != null)
        {
            m_OffsetRotator.localPosition = m_HandOffsetter.localPosition;
            m_OffsetRotator.localRotation = m_HandOffsetter.localRotation;

            transform.rotation = m_HandToOffset.rotation;
            m_HandRotation.position = m_HandToOffset.position;
            m_HandRotation.rotation = m_HandToOffset.rotation;

            m_HandToOffset.parent = m_HandRotation;
            m_HandRotation.Rotate(m_OffsetRotator.localEulerAngles);
            m_HandToOffset.parent = m_OriginalHandParent;

            m_HandOffset.localPosition = (-m_HandToOffset.localPosition + m_OffsetRotator.position) + m_HandToOffset.localPosition;
        }
    }

    
}
