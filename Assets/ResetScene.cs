﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetScene : MonoBehaviour {


    public GameObject m_TrackedDevices;

    public GameObject m_HideObject;
    public GameObject[] m_HideObjects;

    public SkinnedMeshRenderer[] m_HandMesh;

    private void Start()
    {
        Cursor.visible = false;
        //StartCoroutine(HideControllers());
    }

    IEnumerator HideControllers()
    {
        yield return new WaitForSeconds(3);
        m_TrackedDevices.SetActive(false);
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(Application.loadedLevel);
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            Cursor.visible = !Cursor.visible;
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            if (m_TrackedDevices != null)
            {
                m_TrackedDevices.SetActive(!m_TrackedDevices.activeSelf);
            }
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            if (m_HideObject != null)
            {
                m_HideObject.SetActive(!m_HideObject.activeSelf);
            }
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (m_HideObjects != null && m_HideObjects.Length > 0)
            {
                foreach (GameObject obj in m_HideObjects)
                {
                    obj.SetActive(!m_HideObject.activeSelf);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            if (m_HandMesh != null && m_HandMesh.Length > 0)
            {
                foreach (SkinnedMeshRenderer mesh in m_HandMesh)
                {
                    mesh.enabled = !mesh.enabled;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Keypad1) || Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2) || Input.GetKeyDown(KeyCode.Alpha2))
        {
            SceneManager.LoadScene(1, LoadSceneMode.Single);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad3) || Input.GetKeyDown(KeyCode.Alpha3))
        {
            SceneManager.LoadScene(2, LoadSceneMode.Single);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad4) || Input.GetKeyDown(KeyCode.Alpha4))
        {
            SceneManager.LoadScene(3, LoadSceneMode.Single);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad5) || Input.GetKeyDown(KeyCode.Alpha5))
        {
            SceneManager.LoadScene(4, LoadSceneMode.Single);
        }

    }
}
