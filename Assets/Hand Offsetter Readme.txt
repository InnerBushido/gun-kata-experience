1. Drag the prefab next to the hand (ex. Human_RightHand).
2. Drag the hand (Human_RightHand) and put it under "HandOffset".
3. Drag "HandOffsetter" and put it under the hand (Human_RightHand). This will break the prefab. Put it as the top most child under the hand, we will use this to manipulate the hand offset at run time.
4. Click on OffsetCurrent, this has the main script. Drag the hand (Human_RightHand) and assign it to the field "Hand To Offset" in the script.
5. To offset the hand, find the "HandOffset" object and change it's position and rotation fields.
6. Click Play and test that the hands offset correctly.
7. To adjust hand offset, while running in editor, locate the "HandOffsetter" under the hand (Human_RightHand). Adjust the Position/Rotation values until the hand lines up.
8. After hand lines up, copy the component of "HandOffsetter" by clicking the cog in inspector.
9. Stop playing in the editor. Click on "HandOffset". Paste the component values by clicking the cog in inspector. This should save the offset value.

Note: Do not paste component values in "HandOffsetter" since this gets over written during runtime. Make sure to paste values in "HandOffset".