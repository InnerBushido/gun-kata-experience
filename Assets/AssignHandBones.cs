﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HI5;

public class AssignHandBones : MonoBehaviour {

    [Tooltip("Bones attached to hand model")]
    //public Transform[] m_HandBones;
    public List<Transform> m_HandBones;

    public string m_HandPrefix = "Arm";

    public HI5_TransformInstance m_Hi5Hand;

    //[Tooltip("Used to know how many joints to retarget per finger starting from root finger bone.")]
    //public int[] jointsPerFinger = { 4, 4, 4, 4, 4 };

    public List<Quaternion> m_HandQuaternionOffset = new List<Quaternion>();
    public Quaternion mainBodyRotationOffset = new Quaternion();

    private void Start()
    {
        mainBodyRotationOffset = Quaternion.identity;

        if (m_HandBones != null)
        {
            AssignPNHandJoints();
            //for (int i = 0; i < m_HandBones.Count; i++)
            //{
            //    if (m_HandBones[i] != null)
            //    {
            //        m_HandQuaternionOffset.Add(Quaternion.Inverse(mainBodyRotationOffset) * m_HandBones[i].localRotation);
            //    }
            //}

        }

    }

    private void AssignPNHandJoints()
    {
        m_HandQuaternionOffset = new List<Quaternion>();

        m_HandQuaternionOffset.Add(Quaternion.identity);

        ReturnRotation(1);

        ReturnRotation(2);
        ReturnRotation(3);
        ReturnRotation(4);

        m_HandQuaternionOffset.Add(Quaternion.identity);
        ReturnRotation(6);
        ReturnRotation(7);
        ReturnRotation(8);

        m_HandQuaternionOffset.Add(Quaternion.identity);
        ReturnRotation(10);
        ReturnRotation(11);
        ReturnRotation(12);

        m_HandQuaternionOffset.Add(Quaternion.identity);
        ReturnRotation(14);
        ReturnRotation(15);
        ReturnRotation(16);

        m_HandQuaternionOffset.Add(Quaternion.identity);
        ReturnRotation(18);
        ReturnRotation(19);
        ReturnRotation(20);
    }

    private void ReturnRotation(int joint)
    {
        //m_HandQuaternionOffset.Add(Quaternion.Inverse(mainBodyRotationOffset) * m_HandBones[joint].localRotation);
        m_HandQuaternionOffset.Add(m_HandBones[joint].rotation);
    }

    private void Update()
    {
        mainBodyRotationOffset = Quaternion.identity;

        if (m_Hi5Hand != null && m_HandBones != null && m_HandBones.Count > 0)
        {
            //RotateFingerJoints();
            ApplyPNRotationWithRotationalOffset();
        }
    }

    private void ApplyPNRotationWithRotationalOffset()
    {
        ApplyPNHandRotation(m_HandBones, m_Hi5Hand.HandBones, m_HandQuaternionOffset);
    }
    
    private void ApplyPNHandRotation(List<Transform> FingerJoints, Transform[] PNHandJoints, List<Quaternion> HandQuaternionOffset)
    {
        for (int i = 0; i < m_HandBones.Count; i++)
        {
            if (i == 1)
            {
                continue;
            }

            if (i < FingerJoints.Count && i < PNHandJoints.Length && i < HandQuaternionOffset.Count)
            {
                if (FingerJoints[i] != null && PNHandJoints[i] != null && HandQuaternionOffset[i] != null)
                {
                    //FingerJoints[i].localRotation = mainBodyRotationOffset;
                    //FingerJoints[i].localRotation *= PNHandJoints[i].localRotation;
                    //FingerJoints[i].localRotation *= HandQuaternionOffset[i];
                    
                    FingerJoints[i].localRotation = HandQuaternionOffset[i];
                    FingerJoints[i].localRotation *= PNHandJoints[i].localRotation;
                    FingerJoints[i].localRotation *= HandQuaternionOffset[i];

                    //FingerJoints[i].localRotation = Quaternion.identity;
                    //FingerJoints[i].localRotation = PNHandJoints[i].localRotation;

                    //int index = m_Hi5Hand.HandType == Hand.LEFT ? HI5_BindInfoManager.LeftID : HI5_BindInfoManager.RightID;

                    //if (index == -1)
                    //    return;

                    //var device = SteamVR_Controller.Input(index);


                    //if (!device.GetPose().bDeviceIsConnected)
                    //    return;

                    //if (!device.GetPose().bPoseIsValid)
                    //    return;

                    //var pose = new SteamVR_Utils.RigidTransform(device.GetPose().mDeviceToAbsoluteTracking);

                    //FingerJoints[i].localRotation = Quaternion.identity;
                    //FingerJoints[i].localRotation = PNHandJoints[i].localRotation;// Quaternion.Inverse(pose.rot) * PNHandJoints[i].localRotation;

                }
            }
        }
    }

    void RotateFingerJoints()
    {
        for (int i = 0; i < m_HandBones.Count; i++)
        {
            if (m_HandBones[i] != null)
            {
                m_HandBones[i].localRotation = m_Hi5Hand.HandBones[i].localRotation;
            }
        }
    }

}
