﻿using UnityEngine;
using UnityEditor;
using HI5;

[CustomEditor(typeof(AssignHandBones))]
public class LoadHandsOnModel : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AssignHandBones targetScript = (AssignHandBones)target;

        if (targetScript.m_Hi5Hand != null && targetScript.m_Hi5Hand.HandType == Hand.LEFT)
        {
            if (GUILayout.Button("Load LEFT Hand References"))
            {
                Debug.Log("[HI5TransformInstance] - LOAD Left hand Transform references into the bones list!");
                //HI5Helper.AutoBindBones(targetScript.transform, targetScript.m_HandBones, "Human_", HandType.LEFT);
                //HI5_Manager.BindBones(targetScript.transform, targetScript.m_HandBones, targetScript.m_HandPrefix, Hand.LEFT);
                AssignPNHandJoints(targetScript, targetScript.transform, targetScript.m_HandPrefix);
                EditorUtility.SetDirty(targetScript);
            }
        }

        if (targetScript.m_Hi5Hand != null && targetScript.m_Hi5Hand.HandType == Hand.RIGHT)
        {
            if (GUILayout.Button("Load RIGHT Hand References"))
            {
                Debug.Log("[HI5TransformInstance] - LOAD Right hand Transform references into the bones list!");
                //HI5Helper.AutoBindBones(targetScript.transform, targetScript.m_HandBones, "Human_", HandType.RIGHT);
                //HI5_Manager.BindBones(targetScript.transform, targetScript.m_HandBones, targetScript.m_HandPrefix, Hand.RIGHT);
                AssignPNHandJoints(targetScript, targetScript.transform, targetScript.m_HandPrefix);
                EditorUtility.SetDirty(targetScript);
            }
        }
    }

    private void AssignPNHandJoints(AssignHandBones targetScript, Transform handBase, string handPrefix)
    {
        targetScript.m_HandBones = new System.Collections.Generic.List<Transform>();

        targetScript.m_HandBones.Add(null);
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Hand"));

        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Thumb1"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Thumb2"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Thumb3"));
        //targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Thumb4"));

        targetScript.m_HandBones.Add(null);
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Index1"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Index2"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Index3"));
        //targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Index4"));

        targetScript.m_HandBones.Add(null);
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Mid1"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Mid2"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Mid3"));
        //targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Mid4"));

        targetScript.m_HandBones.Add(null);
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Ring1"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Ring2"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Ring3"));
        //targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Ring4"));

        targetScript.m_HandBones.Add(null);
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Pinky1"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Pinky2"));
        targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Pinky3"));
        //targetScript.m_HandBones.Add(handBase.Search(handPrefix + "Pinky4"));
    }
}

public static class Extensions
{
    public static Transform Search(this Transform target, string name)
    {
        if (target.name == name) return target;

        for (int i = 0; i < target.childCount; ++i)
        {
            var result = Search(target.GetChild(i), name);

            if (result != null) return result;
        }

        return null;
    }
}

